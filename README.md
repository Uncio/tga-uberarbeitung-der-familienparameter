# README #

1. RenameSharedParametersAccordingGUIDinSPF
Replaces shared parameters which match parameters in SPF by GUID and don't match by name with parameter with correct parameter name according to the SPF.
Keeps associations with parameters of nested family instances and replaces shared parameter in annotation labels of families category Tag with work around.
Keeps paramaters values of instances of nested families inside the host family.

The tool requests as input folder with families (recursively collects all .rfa files from all subfolders) and SPF file (.txt).

As output tool creates, if neseccary, 3 files in the folder defined for family files:
- "FailedFamilies.txt" - files, where something went wrong
- "FamiliesOver2020.txt" - failed files, which were saved in later version of revit
- "SucceededFiles.csv" - report with success status for each parameter replacement (success, failure, for some cases why failed)

2. ReplaceSharedParameterNotFromSPFWithFamilyParameter
Replaces shared parameters in a family file which are not from a user defined SPF (checked by name and by guid separately) with family parameter with the same name.
Host(top level) families only.
Keeps associations with parameters of nested family instances.
Keeps paramaters values of instances of nested families inside the host family.

The tool requests as input folder with families (recursively collects all .rfa files from all subfolders) and SPF file (.txt).

As output tool creates, if neseccary, 3 files in the folder defined for family files:
- "FailedFamilies.txt" - files, where something went wrong
- "FamiliesOver2020.txt" - failed files, which were saved in later version of revit
- "SucceededFiles.csv" - report with success status for each parameter replacement (success, failure, for some cases why failed)

3. ChangeSharedParameterGUIDAccordingNameInSPF
Replaces shared parameters which match parameters in SPF by name and don't match by Guid with parameter with correct parameter GUID according to the SPF.
Keeps associations with parameters of nested family instances and replaces shared parameter in annotation labels of families category Tag with work around.
Keeps paramaters values of instances of nested families inside the host family.

The tool requests as input folder with families (recursively collects all .rfa files from all subfolders) and SPF file (.txt).

As output tool creates, if neseccary, 3 files in the folder defined for family files:
- "FailedFamilies.txt" - files, where something went wrong
- "FamiliesOver2020.txt" - failed files, which were saved in later version of revit
- "SucceededFiles.csv" - report with success status for each parameter replacement (success, failure, for some cases why failed)

4. RenameFamilyParameters
Renames family parameters in host(top level) family according to .csv file.

The tool requests as input folder with families (recursively collects all .rfa files from all subfolders) and a file with renaming data (.csv)
in format: 1't column - current name, 2'd column - new name.

As output tool creates, if neseccary, 4 files in the folder defined for family files:
- "FailedFamilies.txt" - files, where something went wrong
- "FamiliesOver2020.txt" - failed files, which were saved in later version of revit
- "SucceededFiles.csv" - report with success status for each parameter replacement (success, failure, for some cases why failed)
- "NamesNotFound.csv" - report of current names from csv file which were not found among the family files

Additional tools:
1. FamilyRevitVersionReport
Creates a report of Revit version compliance (2020)

The tool requests as input folder with families (recursively collects all .rfa files from all subfolders).

As output tool creates a csv file in the folder defined for family files:
- "RevVersionReport.csv"

2. NullNameTypesCleaner
Cleans up families from types with no name if there are more then one type.

The tool requests as input folder with families (recursively collects all .rfa files from all subfolders).

As output tool creates, if neseccary, 2 files in the folder defined for family files:
- "FailedFamilies.txt" - files, where something went wrong
- "FamiliesOver2020.txt" - failed files, which were saved in later version of revit

3. RenameTypes
Renames family type according to .csv file.

The tool requests as input folder with families (recursively collects all .rfa files from all subfolders) and a file with renaming data (.csv)
in format: 1't column - family name, 2'd column - current type name, 3'd column - new type name.

As output tool creates, if neseccary, 4 files in the folder defined for family files:
- "FailedFamilies.txt" - files, where something went wrong
- "FamiliesOver2020.txt" - failed files, which were saved in later version of revit
- "SucceededFiles.csv" - report with success status for each parameter replacement (success, failure, for some cases why failed)
- "FamiliesWithoutFile.txt" - report of family names from .csv data which were not found in the family folder
