﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class DeleteParametersFromTemplate : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var doc = commandData.Application.ActiveUIDocument.Document;

            var additionalDialogs = new List<FileFolderModel>
            {
                new FileFolderModel {
                    Path = Properties.Settings.Default.CsvFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "csv files(*.csv) | *.csv",
                    Label = "Datei auswählen..."},
                new FileFolderModel {
                    Path = Properties.Settings.Default.ReportFolder,
                    DialogType = DialogTypeEnum.Folder,
                    Label = "Ordner zum Speichern des Berichts ..."},
            };

            var ui = new CustomUIVM2("Delete Parameters from template", additionalDialogs);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.CsvFilePath = ui.DialogSettings[0].Path;
            Properties.Settings.Default.ReportFolder = ui.DialogSettings[1].Path;
            Properties.Settings.Default.Save();

            var csvPath = Properties.Settings.Default.CsvFilePath;

            var deletingParameters = new List<string>();
            var valuesSet = csvPath.ReadCsvFile();

            foreach (var values in valuesSet)
            {
                if (!deletingParameters.Contains(values[0]))
                {
                    deletingParameters.Add(values[0]);
                }
            }

            var sharedParameterElementsToDelete = new List<ElementId>();
            using (var col = new FilteredElementCollector(doc))
            {
                sharedParameterElementsToDelete = col.OfClass(typeof(SharedParameterElement))
                    .Where(x => deletingParameters.Any(z => z == (x as SharedParameterElement).GuidValue.ToString()))
                    .Select(x => x.Id).ToList();
            }

            using (var col = new FilteredElementCollector(doc))
            {
                var allSharedParameters = col.OfClass(typeof(SharedParameterElement)).Cast<SharedParameterElement>().ToList();
                var parametersNotInTemplate = deletingParameters
                    .Where(x => !allSharedParameters.Any(z => x == z.GuidValue.ToString()))
                    .ToList();
                if (parametersNotInTemplate.Any())
                {
                    string log = String.Empty;
                    parametersNotInTemplate.ForEach(x => log += x + "\n");
                    log.WriteToTxt("FailedFamilies.txt", Properties.Settings.Default.ReportFolder);
                }
            }

            using (var tr = new Transaction(doc, "Deleting parameters"))
            {
                tr.Start();
                doc.Delete(sharedParameterElementsToDelete);
                tr.Commit();
            }

            return Result.Succeeded;
        }
    }
}
