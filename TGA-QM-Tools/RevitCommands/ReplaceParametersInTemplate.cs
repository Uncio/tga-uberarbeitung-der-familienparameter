﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Models;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    //not finished
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    internal class ReplaceParametersInTemplate : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application.Application;
            var doc = commandData.Application.ActiveUIDocument.Document;

            var additionalDialogs = new List<FileFolderModel>
            {
                new FileFolderModel {
                    Path = Properties.Settings.Default.CsvFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "csv files(*.csv) | *.csv",
                    Label = "Datei auswählen..."},
                new FileFolderModel {
                    Path = Properties.Settings.Default.TgaSpfFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "txt files(*.txt) | *.txt",
                    Label = "TGA SPF-Datei auswählen..."},
                new FileFolderModel {
                    Path = Properties.Settings.Default.ReportFolder,
                    DialogType = DialogTypeEnum.Folder,
                    Label = "Ordner zum Speichern des Berichts ..."},
            };

            var ui = new CustomUIVM2("Replace Parameters in template", additionalDialogs);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.CsvFilePath = ui.DialogSettings[0].Path;
            Properties.Settings.Default.TgaSpfFilePath = ui.DialogSettings[1].Path;
            Properties.Settings.Default.ReportFolder = ui.DialogSettings[2].Path;
            Properties.Settings.Default.Save();

            var csvPath = Properties.Settings.Default.CsvFilePath;

            var replacingParameters = new List<ReplacingParameterData>();
            var valuesSet = csvPath.ReadCsvFile();

            foreach (var values in valuesSet)
            {
                if (!replacingParameters.Any(x => x.ParameterGuid == values[1]))
                {
                    replacingParameters.Add(new ReplacingParameterData { ParameterName = values[0], ParameterGuid = values[1]});
                }
            }

            var tempSpf = app.SharedParametersFilename;

            app.SharedParametersFilename = Properties.Settings.Default.TgaSpfFilePath;
            var spf = app.OpenSharedParameterFile();

            foreach(var group in spf.Groups)
            {
                foreach(ExternalDefinition def in group.Definitions)
                {
                    if (replacingParameters.Any(x => x.ParameterName == def.Name || x.ParameterGuid == def.GUID.ToString()))
                    {
                        replacingParameters.FirstOrDefault(x => x.ParameterName == def.Name || x.ParameterGuid == def.GUID.ToString()).Definition = def;
                    }
                }
            }

            //var map = doc.ParameterBindings;
            //var iterator = map.ForwardIterator();
            //while (iterator.MoveNext())
            //{
            //    var binding = iterator.Current;
            //    if (iterator.Key is SharedParameterElement
            //        && replacingParameters.Any(x => x.ParameterGuid == ))
            //}


            return Result.Succeeded;
        }
    }
}
