﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.FailureHandlers;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class RenameTypes : IExternalCommand
    {
        private string familyOver2020 = String.Empty;
        private string failedFamilies = String.Empty;
        private string familiesNotInList = String.Empty;
        private string absentTypes = String.Empty;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application.Application;

            var additionalDialogs = new List<FileFolderModel>
            {
                new FileFolderModel {
                    Path = Properties.Settings.Default.CsvFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "csv files(*.csv) | *.csv",
                    Label = "Datei auswählen..."}
            };

            var ui = new CustomUIVM("Rename Types", additionalDialogs, Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.CsvFilePath = ui.DialogSettings[1].Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;
            var csvPath = Properties.Settings.Default.CsvFilePath;

            if (familyFiles == null || csvPath == null)
            {
                return Result.Cancelled;
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            var renamingSet = new Dictionary<string, List<(string, string)>>();

            var valuesSet = csvPath.ReadCsvFile();

            foreach (var values in valuesSet)
            {
                if (!values[1].Equals(values[2]))
                {
                    var famName = values[0];
                    if (famName.Contains(".rfa"))
                        famName = famName.Remove(famName.LastIndexOf(".rfa"));

                    if (!renamingSet.ContainsKey(famName))
                    {
                        renamingSet.Add(famName, new List<(string, string)>());
                    }

                    renamingSet[famName].Add((values[1], values[2]));
                }
            }

            var table = new List<List<string>>();
            table.Add(new List<string> { "Familienname", "AltTypname", "NeuTypname" });

            var title = "Process files";
            var maxValue = familyFiles.Count;
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    var fileShortName = Path.GetFileName(file);
                    if (fileShortName.Contains(".rfa"))
                        fileShortName = fileShortName.Remove(fileShortName.LastIndexOf(".rfa"));

                    try
                    {
                        var famDoc = app.OpenDocumentFile(file);
                        var types = famDoc.FamilyManager.Types.Cast<FamilyType>();

                        try
                        {
                            if (renamingSet.ContainsKey(fileShortName))
                            {
                                var nameSet = renamingSet[fileShortName];
                                foreach (var pair in nameSet)
                                {
                                    if (!types.Any(x => x.Name == pair.Item2))
                                    {
                                        var typeToRename = types.FirstOrDefault(x => x.Name == pair.Item1);
                                        if (typeToRename == null)
                                        {
                                            absentTypes += file + " / " + pair.Item1 + Environment.NewLine;
                                        }
                                        else
                                        {
                                            using (var tr = new Transaction(famDoc, "Renaming"))
                                            {
                                                FailureHandlingOptions failOpt = tr.GetFailureHandlingOptions();
                                                failOpt.SetFailuresPreprocessor(new FailureHandling());
                                                tr.SetFailureHandlingOptions(failOpt);
                                                FailureHandling.Success = true;
                                                tr.Start();
                                                famDoc.FamilyManager.CurrentType = typeToRename;
                                                famDoc.FamilyManager.RenameCurrentType(pair.Item2);
                                                tr.Commit();
                                                if (FailureHandling.Success)
                                                {
                                                    table.Add(new List<string> { fileShortName, pair.Item1, pair.Item2 });
                                                }
                                                else
                                                {
                                                    table.Add(new List<string> { fileShortName, pair.Item1, pair.Item2, "failed" });
                                                    FailureHandling.Success = true;
                                                }
                                            }
                                        }
                                    }
                                }

                                types = famDoc.FamilyManager.Types.Cast<FamilyType>();
                                if (types.Any(x => !nameSet.Select(z => z.Item2).Any(y => y == x.Name)))
                                {
                                    using (var tr = new Transaction(famDoc, "DeletingNoName"))
                                    {
                                        tr.Start();
                                        try
                                        {
                                            foreach (var type in types.Where(x => !nameSet.Select(z => z.Item2).Any(y => y == x.Name)))
                                            {
                                                famDoc.FamilyManager.CurrentType = type;
                                                famDoc.FamilyManager.DeleteCurrentType();
                                            }
                                            tr.Commit();
                                        }
                                        catch
                                        {
                                            tr.RollBack();
                                        }
                                    }
                                }

                                famDoc.Save();
                            }
                            else
                            {
                                familiesNotInList += file + Environment.NewLine;
                            }

                            types = famDoc.FamilyManager.Types.Cast<FamilyType>();

                            if (types.Any(x => String.IsNullOrEmpty(x.Name) || x.Name == " "))
                            {
                                using (var tr = new Transaction(famDoc, "DeletingNoName"))
                                {
                                    tr.Start();
                                    try
                                    {
                                        foreach (var type in types.Where(x => String.IsNullOrEmpty(x.Name) || x.Name == " "))
                                        {
                                            famDoc.FamilyManager.CurrentType = type;
                                            famDoc.FamilyManager.DeleteCurrentType();
                                        }
                                        tr.Commit();
                                    }
                                    catch
                                    {
                                        tr.RollBack();
                                    }
                                }

                                famDoc.Save();
                            }
                        }
                        catch (Exception ex)
                        {
                            failedFamilies += file + Environment.NewLine;
                        }
                        finally
                        {
                            if (famDoc != null && famDoc.IsValidObject)
                            {
                                famDoc.Close(false);
                            }
                        }

                    }
                    catch
                    {
                        familyOver2020 += file + Environment.NewLine;
                    }

                    pf.Increment();
                }
            }

            var famsOutFiles = String.Empty;
            if (familyFiles.Any())
            {
                var shortFileNames = familyFiles.GetShortFileNames();

                foreach (var famName in renamingSet.Keys)
                {
                    if (!shortFileNames.Contains(famName))
                        famsOutFiles += famName + Environment.NewLine;
                }
            }

            famsOutFiles.WriteToTxt(prefix + "FamiliesWithoutFile.txt");
            failedFamilies.WriteToTxt(prefix + "FailedFamilies.txt");
            familyOver2020.WriteToTxt(prefix + "FamiliesOver2020.txt");
            //familiesNotInList.WriteToTxt("FamiliesNotInCSV.txt");

            table.WriteCsvTo("SucceededFiles.csv");
            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            return Result.Succeeded;
        }
    }
}
