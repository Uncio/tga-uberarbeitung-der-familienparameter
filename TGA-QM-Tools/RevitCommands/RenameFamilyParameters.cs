﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels;
using TGA_QM_Tools.BLModels.FailureHandlers;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class RenameFamilyParameters : IExternalCommand
    {
        private string familyOver2020 = String.Empty;
        private string failedFamilies = String.Empty;
        private List<List<string>> absentParameterNames = new List<List<string>>();
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application.Application;

            var additionalDialogs = new List<FileFolderModel>
            {
                new FileFolderModel {
                    Path = Properties.Settings.Default.CsvFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "csv files(*.csv) | *.csv",
                    Label = "Datei auswählen..."}
            };

            var ui = new CustomUIVM("Rename Family Parameters", additionalDialogs, Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.CsvFilePath = ui.DialogSettings[1].Path;
            Properties.Settings.Default.Save();

            var familyFiles = ui.FamilyFiles;
            var csvPath = Properties.Settings.Default.CsvFilePath;

            if (familyFiles == null || csvPath == null)
            {
                return Result.Cancelled;
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            var renamingSet = new Dictionary<string, string>();
            var valuesSet = csvPath.ReadCsvFile();

            foreach (var values in valuesSet)
            {
                if (!renamingSet.ContainsKey(values[0]))
                {
                    renamingSet.Add(values[0], values[1]);
                }
            }

            var table = new List<List<string>>();
            table.Add(new List<string> { "Familienname", "AltParametername", "NeuParametername" });

            var foundParameters = new List<string>();

            var title = "Process files";
            var maxValue = familyFiles.Count;
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    Document famDoc = null;
                    bool changed = false;

                    try
                    {
                        try
                        {
                            famDoc = app.OpenDocumentFile(file);
                        }
                        catch (Exception ex)
                        {
                            familyOver2020 += file + Environment.NewLine;
                        }

                        var fpHandler = new FamilyParameterHandler(famDoc);
                        var associationHandler = new AssociationsHandler(famDoc);
                        var parameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();

                        using (var trGroup = new TransactionGroup(famDoc, "Renaming family parameters"))
                        {
                            trGroup.Start();

                            try
                            {
                                foreach (var parameter in parameters)
                                {
                                    var row = new List<string>();

                                    parameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();
                                    if (!parameter.IsShared
                                        && (parameter.Definition as InternalDefinition).BuiltInParameter == BuiltInParameter.INVALID
                                        && renamingSet.ContainsKey(parameter.Definition.Name))
                                    {
                                        foundParameters.Add(parameter.Definition.Name);

                                        row.Add(file);
                                        row.Add(parameter.Definition.Name);
                                        row.Add(renamingSet[parameter.Definition.Name]);

                                        if (parameters.Any(x => x.Definition.Name == renamingSet[parameter.Definition.Name]))
                                        {
                                            row.Add("ParamaterWithNewNameExists");
                                        }
                                        else
                                        {
                                            changed = true;
                                            var parameterAssociation = parameter.AssociatedParameters.IsEmpty ? null : new ParameterAssociation(parameter);

                                            using (var tr = new Transaction(famDoc, "Renaming"))
                                            {
                                                FailureHandlingOptions failOpt = tr.GetFailureHandlingOptions();
                                                failOpt.SetFailuresPreprocessor(new FailureHandling());
                                                tr.SetFailureHandlingOptions(failOpt);
                                                FailureHandling.Success = true;

                                                tr.Start();

                                                var newName = renamingSet[parameter.Definition.Name];
                                                var success = fpHandler.RenameParameter(parameter, renamingSet[parameter.Definition.Name]);
                                                associationHandler.RemapRenamedFamilyParameter(parameter, parameterAssociation, newName);

                                                if (success)
                                                {
                                                    success = associationHandler.ReassociateRenamedFamilyParameter(parameterAssociation);

                                                    if (success)
                                                    {
                                                        tr.Commit();
                                                    }
                                                    else
                                                    {
                                                        tr.RollBack();
                                                        row.Add("Failed");
                                                    }
                                                }
                                                else
                                                {
                                                    tr.RollBack();
                                                    row.Add("Failed");
                                                }

                                                if (!FailureHandling.Success)
                                                {
                                                    table.LastOrDefault().Add("failed");
                                                    FailureHandling.Success = true;
                                                    changed = false;
                                                }
                                            }
                                        }

                                        table.Add(row);
                                    }
                                }

                                trGroup.Assimilate();
                            }
                            catch (Exception ex)
                            {
                                trGroup.RollBack();
                                failedFamilies += file + Environment.NewLine;
                            }
                        }

                        if (changed)
                        {
                            famDoc.Save();
                        }
                    }
                    catch (Exception ex)
                    {
                        failedFamilies += file + Environment.NewLine;
                    }
                    finally
                    {
                        if (famDoc != null && famDoc.IsValidObject)
                        {
                            famDoc.Close(false);
                        }
                    }
                    pf.Increment();
                }
            }

            absentParameterNames.AddRange(renamingSet.Where(x => !foundParameters.Contains(x.Key)).Select(x => new List<string> { x.Key }));

            failedFamilies.WriteToTxt(prefix + "FailedFamilies.txt");
            familyOver2020.WriteToTxt(prefix + "FamiliesOver2020.txt");
            table.WriteCsvTo(prefix + "SucceededFiles.csv");
            absentParameterNames.WriteCsvTo(prefix + "NamesNotFound.csv");
            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            return Result.Succeeded;
        }
    }
}
