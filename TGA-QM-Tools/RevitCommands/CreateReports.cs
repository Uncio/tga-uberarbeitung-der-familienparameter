﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    public class CreateReports : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var view = new ReportsView(commandData.Application);
            view.ShowDialog();

            return Result.Succeeded;
        }
    }
}