﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels;
using TGA_QM_Tools.BLModels.FailureHandlers;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.BLModels.SPF_logic;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class ChangeSharedParameterGUIDAccordingNameInSPF : IExternalCommand
    {
        private ReadingSPF spfReader;
        private string familyOver2020 = String.Empty;
        private string failedFamilies = String.Empty;
        private List<string> parametersWithDifferentGuids = new List<string>();
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application.Application;
            spfReader = new ReadingSPF(app);

            var additionalDialogs = new List<FileFolderModel>
            {
                new FileFolderModel {
                    Path = Properties.Settings.Default.SpfFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "txt files(*.txt) | *.txt",
                    Label = "SPF-Datei auswählen..."}
            };

            var ui = new CustomUIVM("Change Shared Parameter GUID According Name In SPF", additionalDialogs, Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.SpfFilePath = ui.DialogSettings[1].Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;
            var spfFile = Properties.Settings.Default.SpfFilePath;

            if (familyFiles == null || spfFile == null)
            {
                return Result.Cancelled;
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            var table = new List<List<string>>();
            table.Add(new List<string> { "Familienname", "AltParameterGuid", "NeuParameterGuid", "Parametername" });

            if (!spfReader.ChangeSPF(spfFile))
            {
                TaskDialog.Show("Umbenennung gemeinsam genutzter Parameter", "Gemeinsame Parameterdatei ist falsch");
                return Result.Cancelled;
            }

            var title = "Process files";
            var maxValue = familyFiles.Count;
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    Document famDoc = null;
                    bool changed = false;
                    var tempTable = new List<List<string>>();

                    try
                    {
                        try
                        {
                            famDoc = app.OpenDocumentFile(file);
                        }
                        catch (Exception ex)
                        {
                            familyOver2020 += file + Environment.NewLine;
                        }

                        var spHandler = new SharedParametersHandler(famDoc);
                        var associationHandler = new AssociationsHandler(famDoc);


                        if (famDoc.OwnerFamily.FamilyCategory.CategoryType == CategoryType.Annotation
                                && famDoc.OwnerFamily.FamilyCategory.Id.IntegerValue != (int)BuiltInCategory.OST_GenericAnnotation)
                        {
                            var hostParameterHandler = new AnnotationHostFamiliesHandler(famDoc);
                            var spElements = new List<SharedParameterElement>();
                            using (var col = new FilteredElementCollector(famDoc).OfClass(typeof(SharedParameterElement)))
                            {
                                spElements.AddRange(col.Where(x => spfReader.Definitions.Any(z => z.GUID != (x as SharedParameterElement).GuidValue
                                    && z.Name == (x as SharedParameterElement).Name)).Cast<SharedParameterElement>());
                            }
                            var success = hostParameterHandler.ReplaceSharedParametersWithFake(spElements.Select(x => x.GuidValue)
                                                                                                         .ToList(), byNameOnly: true,
                                                                                                         out tempTable,
                                                                                                         out bool nestedChanged);
                            changed = nestedChanged;

                            if (success == false)
                            {
                                if (famDoc != null && famDoc.IsValidObject)
                                {
                                    famDoc.Close(false);
                                }
                                failedFamilies += file + Environment.NewLine;
                            }

                            success = hostParameterHandler.ReplaceDummyParametersWithRenamedParameterByNameWithTrans(tempTable);

                            if (success == false)
                            {
                                if (famDoc != null && famDoc.IsValidObject)
                                {
                                    famDoc.Close(false);
                                }
                                failedFamilies += file + Environment.NewLine;
                            }
                        }
                        else
                        {
                            var nestedAnnotationsHandler = new AnnotationNestedFamiliesHandler(famDoc);

                            var parameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();

                            parametersWithDifferentGuids = spfReader.Definitions.Where(x => parameters.Any(z => z.IsShared && z.GUID != x.GUID && z.Definition.Name == x.Name))
                                .Select(x => x.Name).ToList();
                            var oldGuids = parameters.Where(x => x.IsShared && spfReader.Definitions.Any(z => z.GUID != x.GUID && z.Name == x.Definition.Name))
                                .Select(x => x.GUID).ToList();
                            var nestedSuccess = nestedAnnotationsHandler.ReplaceSharedParametersWithFake(
                                oldGuids,
                                out List<ParameterAssociation> nestedFamiliesAssociations,
                                out bool nestedChanged,
                                out List<Guid> toDeleteInMain);
                            changed = nestedChanged;

                            if (nestedSuccess == false)
                            {
                                if (famDoc != null && famDoc.IsValidObject)
                                {
                                    famDoc.Close(false);
                                }
                                failedFamilies += file + Environment.NewLine;
                            }

                            using (var trGroup = new TransactionGroup(famDoc, "Replacing shared parameters"))
                            {
                                trGroup.Start();

                                try
                                {
                                    tempTable.Clear();
                                    foreach (var parameter in parameters)
                                    {
                                        var row = new List<string>();

                                        if (parameter.IsShared && spfReader.Definitions.Any(x => x.Name == parameter.Definition.Name && x.GUID != parameter.GUID))
                                        {
                                            changed = true;
                                            var parameterAssociation = parameter.AssociatedParameters.IsEmpty ? null : new ParameterAssociation(parameter);

                                            var extDef = spfReader.Definitions.FirstOrDefault(x => x.Name == parameter.Definition.Name);

                                            row.Add(file);
                                            row.Add(parameter.GUID.ToString());

                                            using (var tr = new Transaction(famDoc, "Replacing"))
                                            {
                                                FailureHandlingOptions failOpt = tr.GetFailureHandlingOptions();
                                                failOpt.SetFailuresPreprocessor(new FailureHandling());
                                                tr.SetFailureHandlingOptions(failOpt);
                                                FailureHandling.Success = true;

                                                tr.Start();

                                                FamilyParameter newParameter = null;

                                                if (parameter.Definition.ParameterType == extDef.ParameterType)
                                                {
                                                    newParameter = spHandler.ReplaceWithSharedParameterSameType(parameter, extDef);
                                                    associationHandler.RemapReplacedSharedWithDifferentGuids(newParameter, parameterAssociation);
                                                }
                                                else
                                                {
                                                    row.Add(extDef.Name);
                                                    row.Add(parameter.GUID.ToString());
                                                    row.Add("failed - different parameter types");
                                                }

                                                if (newParameter != null)
                                                {
                                                    //todo to examine
                                                    if (parameterAssociation != null && nestedFamiliesAssociations.Any(x => oldGuids.Any(z => z == x.ParameterGuid)))
                                                    {
                                                        parameterAssociation.AssociatedParameters =
                                                            parameterAssociation.AssociatedParameters.Where(x => !oldGuids.Any(z => z == x.ParameterGuid)).ToList();
                                                    }
                                                    var success = associationHandler.ReassociateRenamedSharedParameter(parameterAssociation);

                                                    if (success)
                                                    {
                                                        tr.Commit();
                                                        row.Add(newParameter.Definition.Name);
                                                        row.Add(newParameter.GUID.ToString());
                                                    }
                                                    else
                                                    {
                                                        tr.RollBack();
                                                        row.Add("Failed");
                                                        row.Add(parameter.GUID.ToString());
                                                    }
                                                }
                                                else
                                                {
                                                    tr.RollBack();
                                                    row.Add("Failed");
                                                    row.Add(parameter.GUID.ToString());
                                                }

                                                if (!FailureHandling.Success)
                                                {
                                                    table.LastOrDefault().Add("failed");
                                                    FailureHandling.Success = true;
                                                    changed = false;
                                                }
                                            }

                                            tempTable.Add(row);
                                        }
                                    }

                                    trGroup.Assimilate();
                                }
                                catch (Exception ex)
                                {
                                    trGroup.RollBack();
                                    failedFamilies += file + Environment.NewLine;
                                }
                            }

                            nestedSuccess = nestedAnnotationsHandler.ReplaceDummyParametersWithRenamedParameterByName();

                            if (nestedSuccess == false)
                            {
                                if (famDoc != null && famDoc.IsValidObject)
                                {
                                    famDoc.Close(false);
                                }
                                failedFamilies += file + Environment.NewLine;
                            }

                            using (var tr = new Transaction(famDoc, "Renassociation"))
                            {
                                tr.Start();

                                var restoringSuccedded = nestedAnnotationsHandler.RestoreAssociationsAndValues(byNameOnly: true);

                                if (restoringSuccedded == false)
                                {
                                    tr.RollBack();
                                    if (famDoc != null && famDoc.IsValidObject)
                                    {
                                        famDoc.Close(false);
                                    }
                                    failedFamilies += file + Environment.NewLine;
                                }
                                else
                                {
                                    tr.Commit();
                                }
                            }

                        }

                        if (changed)
                        {
                            famDoc.Save();
                        }
                    }
                    catch (Exception ex)
                    {
                        failedFamilies += file + Environment.NewLine;
                    }
                    finally
                    {
                        if (famDoc != null && famDoc.IsValidObject)
                        {
                            famDoc.Close(false);
                        }
                    }
                    table.AddRange(tempTable);

                    pf.Increment();
                }
            }

            failedFamilies.WriteToTxt(prefix + "FailedFamilies.txt");
            familyOver2020.WriteToTxt(prefix + "FamiliesOver2020.txt");
            table.WriteCsvTo(prefix + "SucceededFiles.csv");
            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();


            return Result.Succeeded;
        }
    }
}
