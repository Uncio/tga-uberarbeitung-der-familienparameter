﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels;
using TGA_QM_Tools.BLModels.FailureHandlers;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.BLModels.SPF_logic;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class RenameSharedParametersAccordingGUIDinSPF : IExternalCommand
    {
        private ReadingSPF spfReader;
        private string familyOver2020 = String.Empty;
        private string failedFamilies = String.Empty;
        private List<Guid> renamedParametersGuids = new List<Guid>();
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application.Application;
            spfReader = new ReadingSPF(app);

            var additionalDialogs = new List<FileFolderModel>
            {
                new FileFolderModel {
                    Path = Properties.Settings.Default.SpfFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "txt files(*.txt) | *.txt",
                    Label = "SPF-Datei auswählen..."}
            };

            var ui = new CustomUIVM("Rename Shared Parameters According GUID in SPF", additionalDialogs, Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.SpfFilePath = ui.DialogSettings[1].Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;
            var spfFile = Properties.Settings.Default.SpfFilePath;

            if (familyFiles == null || familyFiles.Count == 0)
            {
                return Result.Cancelled;
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            var table = new List<List<string>>();
            table.Add(new List<string> { "Familienname", "AltParametername", "NeuParametername", "Guid" });

            if (!spfReader.ChangeSPF(spfFile))
            {
                TaskDialog.Show("Umbenennung gemeinsam genutzter Parameter", "Gemeinsame Parameterdatei ist falsch");
                return Result.Cancelled;
            }

            var title = "Process files";
            var maxValue = familyFiles.Count;
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    Document famDoc = null;
                    var tempTable = new List<List<string>>();
                    bool changed = false;

                    try
                    {
                        try
                        {
                            famDoc = app.OpenDocumentFile(file);
                        }
                        catch (Exception ex)
                        {
                            familyOver2020 += file + Environment.NewLine;
                        }

                        var spHandler = new SharedParametersHandler(famDoc);
                        var associationHandler = new AssociationsHandler(famDoc);
                        bool mainChanged = false;

                        if (famDoc.OwnerFamily.FamilyCategory.CategoryType == CategoryType.Annotation
                                && famDoc.OwnerFamily.FamilyCategory.Id.IntegerValue != (int)BuiltInCategory.OST_GenericAnnotation)
                        {
                            var hostParameterHandler = new AnnotationHostFamiliesHandler(famDoc);
                            var success = hostParameterHandler.ReplaceSharedParametersWithFake(spfReader.Definitions.Select(x => x.GUID).ToList(),
                                                                                               byNameOnly: false,
                                                                                               out tempTable,
                                                                                               out bool nestedChanged);
                            changed = nestedChanged;

                            if (success == false)
                            {
                                if (famDoc != null && famDoc.IsValidObject)
                                {
                                    famDoc.Close(false);
                                }
                                failedFamilies += file + Environment.NewLine;
                            }

                            success = hostParameterHandler.ReplaceDummyParametersWithRenamedParameterWithTrans(tempTable);

                            if (success == false)
                            {
                                if (famDoc != null && famDoc.IsValidObject)
                                {
                                    famDoc.Close(false);
                                }
                                failedFamilies += file + Environment.NewLine;
                            }

                            changed = true;
                        }
                        else
                        {
                            var nestedAnnotationsHandler = new AnnotationNestedFamiliesHandler(famDoc);

                            var parameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();

                            var spElements = new List<SharedParameterElement>();
                            using (var col = new FilteredElementCollector(famDoc))
                            {
                                spElements = col.OfClass(typeof(SharedParameterElement)).Cast<SharedParameterElement>().ToList();
                            }                           

                            renamedParametersGuids = spfReader.Definitions.Where(x => spElements.Any(z => z.GuidValue == x.GUID && z.Name != x.Name))
                                    .Select(x => x.GUID).ToList();
                            var nestedSuccess = nestedAnnotationsHandler.ReplaceSharedParametersWithFake(
                                renamedParametersGuids,
                                out List<ParameterAssociation> nestedFamiliesAssociations,
                                out bool nestedChanged,
                                out List<Guid> spToDelete );
                            changed = nestedChanged;
                            var spElementsToDelete = spElements.Where(x => !parameters.Any(y => y.IsShared && y.GUID == x.GuidValue) && spToDelete.Any(z => z == x.GuidValue));

                            if (nestedSuccess == false)
                            {
                                if (famDoc != null && famDoc.IsValidObject)
                                {
                                    famDoc.Close(false);
                                }
                                failedFamilies += file + Environment.NewLine;
                            }

                            using (var tr = new Transaction(famDoc, "Renaming"))
                            {
                                tr.Start();

                                if (spElementsToDelete.Any())
                                {
                                    famDoc.Delete(spElementsToDelete.Select(x => x.Id).ToList());
                                }

                                tr.Commit();
                            }

                            using (var trGroup = new TransactionGroup(famDoc, "Renaming shared parameters"))
                            {
                                trGroup.Start();

                                try
                                {
                                    tempTable.Clear();
                                    foreach (var parameter in parameters)
                                    {
                                        var row = new List<string>();

                                        if (parameter.IsShared && spfReader.Definitions.Any(x => x.GUID == parameter.GUID && parameter.Definition.Name != x.Name))
                                        {
                                            parameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();
                                            changed = true;
                                            mainChanged = true;

                                            var parameterAssociation = parameter.AssociatedParameters.IsEmpty ? null : new ParameterAssociation(parameter);

                                            var extDef = spfReader.Definitions.FirstOrDefault(x => x.GUID == parameter.GUID);

                                            row.Add(file);
                                            row.Add(parameter.Definition.Name);

                                            if (!parameters.Any(x => x.Definition.Name == extDef.Name))
                                            {

                                                using (var tr = new Transaction(famDoc, "Renaming"))
                                                {
                                                    FailureHandlingOptions failOpt = tr.GetFailureHandlingOptions();
                                                    failOpt.SetFailuresPreprocessor(new FailureHandling());
                                                    tr.SetFailureHandlingOptions(failOpt);
                                                    FailureHandling.Success = true;
                                                    tr.Start();

                                                    FamilyParameter newParameter = null;

                                                    if (parameter.Definition.ParameterType == extDef.ParameterType)
                                                    {
                                                        newParameter = spHandler.ReplaceWithSharedParameterSameType(parameter, extDef);
                                                    }
                                                    else
                                                    {
                                                        row.Add(extDef.Name);
                                                        row.Add(parameter.GUID.ToString());
                                                        row.Add("failed - different parameter types");
                                                        //newParameter = spHandler.ReplaceWithSharedParameterDifferentType(parameter, extDef);
                                                    }

                                                    if (newParameter != null)
                                                    {
                                                        if (parameterAssociation != null && nestedFamiliesAssociations.Any(x => x.ParameterGuid == newParameter.GUID))
                                                        {
                                                            parameterAssociation.AssociatedParameters =
                                                                parameterAssociation.AssociatedParameters.Where(x => x.ParameterGuid != newParameter.GUID).ToList();
                                                        }
                                                        var success = associationHandler.ReassociateRenamedSharedParameter(parameterAssociation);

                                                        if (success)
                                                        {
                                                            tr.Commit();
                                                            row.Add(newParameter.Definition.Name);
                                                            row.Add(newParameter.GUID.ToString());
                                                        }
                                                        else
                                                        {
                                                            tr.RollBack();
                                                            row.Add("ReassociationFailed");
                                                            row.Add(parameter.GUID.ToString());
                                                        }
                                                    }
                                                    else
                                                    {
                                                        tr.RollBack();
                                                        row.Add("ParameterReplacementFailed");
                                                        row.Add(parameter.GUID.ToString());
                                                    }

                                                    if (!FailureHandling.Success)
                                                    {
                                                        table.LastOrDefault().Add("failed");
                                                        FailureHandling.Success = true;
                                                        changed = false;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                row.Add("NameAlreadyExists");
                                                row.Add(parameter.GUID.ToString());
                                            }

                                            tempTable.Add(row);
                                        }
                                    }

                                    trGroup.Assimilate();
                                }
                                catch (Exception ex)
                                {
                                    trGroup.RollBack();
                                    failedFamilies += file + Environment.NewLine;
                                }
                            }

                            nestedSuccess = nestedAnnotationsHandler.ReplaceDummyParametersWithRenamedParameter();

                            if (nestedSuccess == false)
                            {
                                if (famDoc != null && famDoc.IsValidObject)
                                {
                                    famDoc.Close(false);
                                }
                                failedFamilies += file + Environment.NewLine;
                            }

                            using (var tr = new Transaction(famDoc, "Renassociation"))
                            {
                                tr.Start();

                                var restoringSuccedded = nestedAnnotationsHandler.RestoreAssociationsAndValues();

                                if (restoringSuccedded == false)
                                {
                                    tr.RollBack();
                                    if (famDoc != null && famDoc.IsValidObject)
                                    {
                                        famDoc.Close(false);
                                    }
                                    failedFamilies += file + Environment.NewLine;
                                }
                                else
                                {
                                    tr.Commit();
                                }
                            }

                        }

                        if (changed)
                        {
                            famDoc.Save();
                            if (!mainChanged)
                            {
                                table.Add(new List<string> { famDoc.Title, "renaming in nested families only" });
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        failedFamilies += file + Environment.NewLine;
                    }
                    finally
                    {
                        if (famDoc != null && famDoc.IsValidObject)
                        {
                            famDoc.Close(false);
                        }
                    }

                    table.AddRange(tempTable);
                    pf.Increment();
                }
            }

            spfReader.RestoreOriginalSpf();

            failedFamilies.WriteToTxt(prefix + "FailedFamilies.txt");
            familyOver2020.WriteToTxt(prefix + "FamiliesOver2020.txt");
            table.WriteCsvTo(prefix + "SucceededFiles.csv");
            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            return Result.Succeeded;
        }
    }
}
