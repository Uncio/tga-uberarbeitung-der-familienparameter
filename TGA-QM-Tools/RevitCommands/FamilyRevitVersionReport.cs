﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class FamilyRevitVersionReport : IExternalCommand
    {
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application.Application;

            var ui = new CustomUIVM("Family Revit Version Report", new List<FileFolderModel>(), Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;

            if (familyFiles == null)
            {
                return Result.Cancelled;
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            var table = new List<List<string>>();
            table.Add(new List<string> { "Familienname", "RevitVersion" });

            var title = "Process files";
            var maxValue = familyFiles.Count();
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    var row = new List<string>();
                    row.Add(file);
                    try
                    {
                        var famFile = app.OpenDocumentFile(file);
                        famFile.Close(false);
                        row.Add("2020");
                    }
                    catch
                    {
                        row.Add("mehr als 2020");
                    }
                    table.Add(row);

                    pf.Increment();
                }
            }

            table.WriteCsvTo(prefix + "RevVersionReport.csv");

            return Result.Succeeded;
        }
    }
}
