﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class NullNameTypesCleaner : IExternalCommand
    {
        private string familyOver2020 = String.Empty;
        private string failedFamilies = String.Empty;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application.Application;

            var ui = new CustomUIVM("NullNameTypesCleaner", new List<FileFolderModel>(), Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings.FirstOrDefault().Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;

            if (familyFiles == null || familyFiles.Count == 0)
            {
                return Result.Cancelled;
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            var title = "Process files";
            var maxValue = familyFiles.Count;
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    try
                    {
                        var famDoc = app.OpenDocumentFile(file);
                        try
                        {
                            var types = famDoc.FamilyManager.Types.Cast<FamilyType>();

                            if (types.Any(x => String.IsNullOrEmpty(x.Name) || x.Name == " "))
                            {
                                using (var tr = new Transaction(famDoc, "DeletingNoName"))
                                {
                                    tr.Start();
                                    foreach (var type in types.Where(x => String.IsNullOrEmpty(x.Name) || x.Name == " "))
                                    {
                                        famDoc.FamilyManager.CurrentType = type;
                                        famDoc.FamilyManager.DeleteCurrentType();
                                    }
                                    tr.Commit();
                                }

                                famDoc.Save();
                            }
                        }
                        catch (Exception ex)
                        {
                            failedFamilies += file + Environment.NewLine;
                        }
                        finally
                        {
                            if (famDoc != null && famDoc.IsValidObject)
                            {
                                famDoc.Close(false);
                            }
                        }
                    }
                    catch
                    {
                        familyOver2020 += file + Environment.NewLine;
                    }
                    pf.Increment();
                }
            }

            failedFamilies.WriteToTxt(prefix + "FailedFamilies.txt");
            familyOver2020.WriteToTxt(prefix + "FamiliesOver2020.txt");
            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            return Result.Succeeded;
        }
    }
}
