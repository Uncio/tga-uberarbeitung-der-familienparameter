﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels;
using TGA_QM_Tools.BLModels.FailureHandlers;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.BLModels.SPF_logic;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.RevitCommands
{
    [Autodesk.Revit.Attributes.Transaction(Autodesk.Revit.Attributes.TransactionMode.Manual)]
    class ReplaceSharedParameterNotFromSPFWithFamilyParameter : IExternalCommand
    {
        private ReadingSPF spfReader;
        private string familyOver2020 = String.Empty;
        private string failedFamilies = String.Empty;

        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elements)
        {
            var app = commandData.Application.Application;
            spfReader = new ReadingSPF(app);

            var additionalDialogs = new List<FileFolderModel>
            {
                new FileFolderModel {
                    Path = Properties.Settings.Default.SpfFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "txt files(*.txt) | *.txt",
                    Label = "SPF-Datei auswählen..."}
            };

            var ui = new CustomUIVM("Replace Shared Parameters Not From SPF With Family Parameter", additionalDialogs, Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                return Result.Cancelled;
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.SpfFilePath = ui.DialogSettings[1].Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;
            var spfFile = Properties.Settings.Default.SpfFilePath;

            if (familyFiles == null || spfFile == null)
            {
                return Result.Cancelled;
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            var table = new List<List<string>>();
            table.Add(new List<string> { "Familienname", "ReplacedParameterName", "ReplacedParameterGuid" });

            if (!spfReader.ChangeSPF(spfFile))
            {
                TaskDialog.Show("Umbenennung gemeinsam genutzter Parameter", "Gemeinsame Parameterdatei ist falsch");
                return Result.Cancelled;
            }

            var title = "Process files";
            var maxValue = familyFiles.Count;
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    Document famDoc = null;
                    bool changed = false;
                    var tempTable = new List<List<string>>();

                    try
                    {
                        try
                        {
                            famDoc = app.OpenDocumentFile(file);
                        }
                        catch (Exception ex)
                        {
                            familyOver2020 += file + Environment.NewLine;
                        }

                        var spHandler = new SharedParametersHandler(famDoc);
                        var associationHandler = new AssociationsHandler(famDoc);

                        var parameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();

                        var sharedParametersNotFromSPF = parameters.Where(z => z.IsShared && !spfReader.Definitions
                            .Any(x => z.GUID == x.GUID || z.Definition.Name == x.Name))
                            .ToList();

                        if (sharedParametersNotFromSPF.Any())
                        {
                            changed = true;
                            try
                            {
                                tempTable.Clear();
                                foreach (var parameter in sharedParametersNotFromSPF)
                                {
                                    var row = new List<string>();

                                    var parameterAssociation = parameter.AssociatedParameters.IsEmpty ? null : new ParameterAssociation(parameter);

                                    row.Add(file);
                                    row.Add(parameter.Definition.Name);
                                    row.Add(parameter.GUID.ToString());

                                    using (var tr = new Transaction(famDoc, "Renaming"))
                                    {
                                        FailureHandlingOptions failOpt = tr.GetFailureHandlingOptions();
                                        failOpt.SetFailuresPreprocessor(new FailureHandling());
                                        tr.SetFailureHandlingOptions(failOpt);
                                        FailureHandling.Success = true;

                                        tr.Start();
                                        var newParameter = spHandler.ReplaceWithFamilyParameterSameName(parameter);

                                        if (parameterAssociation != null)
                                        {
                                            associationHandler.RemapReplacedSharedWithFamilyParameter(newParameter, parameterAssociation);
                                        }

                                        if (newParameter != null)
                                        {
                                            if (parameterAssociation != null)
                                            {
                                                var success = associationHandler.ReassociateReplacedSharedWithFamilyParameter(parameterAssociation);

                                                if (success)
                                                {
                                                    tr.Commit();
                                                }
                                                else
                                                {
                                                    tr.RollBack();
                                                    row.Add("ReassociationFailed");
                                                }
                                            }
                                            else
                                            {
                                                tr.Commit();
                                            }
                                        }
                                        else
                                        {
                                            tr.RollBack();
                                            row.Add("Failed");
                                        }

                                        if (!FailureHandling.Success)
                                        {
                                            table.LastOrDefault().Add("failed");
                                            FailureHandling.Success = true;
                                            changed = false;
                                        }
                                    }

                                    tempTable.Add(row);
                                }
                            }
                            catch (Exception ex)
                            {
                                changed = false;
                                failedFamilies += file + Environment.NewLine;
                            }

                            if (changed)
                            {
                                famDoc.Save();
                            }
                        }
                        else
                        {
                            table.Add(new List<string> { file, "nothing changed" });
                        }
                    }
                    catch (Exception ex)
                    {
                        failedFamilies += file + Environment.NewLine;
                    }
                    finally
                    {
                        if (famDoc != null && famDoc.IsValidObject)
                        {
                            famDoc.Close(false);
                        }
                    }
                    table.AddRange(tempTable);
                    pf.Increment();
                }
            }

            spfReader.RestoreOriginalSpf();

            failedFamilies.WriteToTxt(prefix + "FailedFamilies.txt");
            familyOver2020.WriteToTxt(prefix + "FamiliesOver2020.txt");
            table.WriteCsvTo(prefix + "SucceededFiles.csv");
            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            return Result.Succeeded;
        }
    }
}
