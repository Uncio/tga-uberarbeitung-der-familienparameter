﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.Models
{
    internal class ReplacingParameterData
    {
        public string ParameterName { get; set; }
        public string ParameterGuid { get; set; }
        public ExternalDefinition Definition { get; set; }
        public CategorySet Categories { get; set; }
        public BuiltInParameterGroup ParameterGroup { get; set; }
    }
}
