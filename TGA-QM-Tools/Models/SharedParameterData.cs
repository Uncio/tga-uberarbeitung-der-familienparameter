﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.Models
{
    internal class SharedParameterData
    {
        public SharedParameterElement SharedParameter { get; set; }
        public bool IsInFamily { get; set; } = false;
        public bool IsFamilyInstance { get; set; } = false;

        public bool IsProject { get; set; } = false;
        public bool IsProjectInstance { get; set; } = false;
        public List<BuiltInCategory> Categories { get; set; }

        public bool IsInFilters { get; set; } = false;
        public string FilterNames { get; set; }
        public bool IsInSchedules { get; set; } = false;
        public string ScheduleNames { get; set; }
        public bool IsTGAspfName { get; set; } = false;
        public bool IsTGAspfGuid { get; set; } = false;
        public bool IsBAUspfName { get; set; } = false ;
        public bool IsBAUspfGuid { get; set; } = false ;

        public SharedParameterData()
        {
            Categories = new List<BuiltInCategory>();
        }
    }
}
