﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.Models
{
    class ViewData
    {
        public string ViewName { get; set; }
    }
    class FilterData
    {
        public string FilterName { get; set; }
        public Rules Rules { get; set; }
        public int FilterId { get; set; }

        public List<string> Parameters { get; set; }
        public FilterData()
        {
            Parameters = new List<string>();
        }
    }

    public class Rules
    {
        public List<And> AND { get; set; }
        public List<Or> OR { get; set; }


        public override bool Equals(object obj)
        {
            var item = obj as Rules;

            if (!(this.AND == null && item.AND == null) && !(this.AND != null && item.AND != null))
                if ((this.AND == null || item.AND == null) || !this.AND.Equals(item.AND)) return false;
            if (!(this.OR == null && item.OR == null) && !(this.OR != null && item.OR != null))
                if ((this.OR == null || item.OR == null) || !this.OR.Equals(item.OR)) return false;

            if (this.AND != null)
            {
                if (this.AND.Count() == item.AND.Count)
                    for (int i = 0; i < this.AND.Count(); i++)
                    {
                        if (!this.AND[i].Equals(item.AND[i])) return false;
                    }
                else return false;

            }

            if (this.OR != null)
            {
                if (this.OR.Count() == item.OR.Count)
                    for (int i = 0; i < this.OR.Count(); i++)
                    {
                        if (!this.OR[i].Equals(item.OR[i])) return false;
                    }
                else return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            var hashCode = -646120262;
            hashCode = hashCode * -1521134295 + EqualityComparer<List<And>>.Default.GetHashCode(AND);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Or>>.Default.GetHashCode(OR);
            return hashCode;
        }
    }

    public class And
    {
        public string Parameter { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public List<Or> OR { get; set; }
        public List<And> AND { get; set; }



        public override bool Equals(object obj)
        {
            var item = obj as And;
            if (!(this.Parameter == null && item.Parameter == null))
            {
                if (this.Parameter == null || item.Parameter == null) return false;
                else if (this.Parameter.ToLower() != item.Parameter.ToLower()) return false;
            }


            if (this.Type != item.Type) return false;
            if (this.Value != item.Value) return false;
            if (!(this.AND == null && item.AND == null) && !(this.AND != null && item.AND != null))
                if ((this.AND == null || item.AND == null) || !this.AND.Equals(item.AND)) return false;
            if (!(this.OR == null && item.OR == null) && !(this.OR != null && item.OR != null))
                if ((this.OR == null || item.OR == null) || !this.OR.Equals(item.OR)) return false;

            if (this.AND != null)
            {
                if (this.AND.Count() == item.AND.Count)
                    for (int i = 0; i < this.AND.Count(); i++)
                    {
                        if (!this.AND[i].Equals(item.AND[i])) return false;
                    }
                else return false;

            }

            if (this.OR != null)
            {
                if (this.OR.Count() == item.OR.Count)
                    for (int i = 0; i < this.OR.Count(); i++)
                    {
                        if (!this.OR[i].Equals(item.OR[i])) return false;
                    }
                else return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            var hashCode = -747141271;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Parameter);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Type);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Value);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Or>>.Default.GetHashCode(OR);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<And>>.Default.GetHashCode(AND);
            return hashCode;
        }
    }


    public class Or
    {
        public string Parameter { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public List<Or> OR { get; set; }
        public List<And> AND { get; set; }

        public override bool Equals(object obj)
        {
            var item = obj as Or;
            if (!(this.Parameter == null && item.Parameter == null))
            {
                if (this.Parameter == null || item.Parameter == null) return false;
                else if (this.Parameter.ToLower() != item.Parameter.ToLower()) return false;
            }
            if (this.Type != item.Type) return false;
            if (this.Value != item.Value) return false;
            if (!(this.AND == null && item.AND == null) && !(this.AND != null && item.AND != null))
                if ((this.AND == null || item.AND == null) || !this.AND.Equals(item.AND)) return false;
            if (!(this.OR == null && item.OR == null) && !(this.OR != null && item.OR != null))
                if ((this.OR == null || item.OR == null) || !this.OR.Equals(item.OR)) return false;

            if (this.AND != null)
            {
                if (this.AND.Count() == item.AND.Count)
                    for (int i = 0; i < this.AND.Count(); i++)
                    {
                        if (!this.AND[i].Equals(item.AND[i])) return false;
                    }
                else return false;

            }

            if (this.OR != null)
            {
                if (this.OR.Count() == item.OR.Count)
                    for (int i = 0; i < this.OR.Count(); i++)
                    {
                        if (!this.OR[i].Equals(item.OR[i])) return false;
                    }
                else return false;
            }

            return true;


        }

        public override int GetHashCode()
        {
            var hashCode = -747141271;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Parameter);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Type);
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(Value);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<Or>>.Default.GetHashCode(OR);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<And>>.Default.GetHashCode(AND);
            return hashCode;
        }
    }
}
