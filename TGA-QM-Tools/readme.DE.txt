1. RenameSharedParametersAccordingGUIDinSPF
Ersetzt gemeinsam genutzte Parameter, die mit den Parametern im SPF durch GUID übereinstimmen und nicht durch den Namen, durch Parameter mit dem korrekten Parameternamen gemäß SPF.
Behält Assoziationen mit Parametern von verschachtelten Familieninstanzen bei und ersetzt gemeinsam genutzte Parameter in Beschriftungen der Familienkategorie Tag durch Workaround.
Behält Parameterwerte von Instanzen verschachtelter Familien innerhalb der Host-Familie bei.

Das Tool verlangt als Eingabe einen Ordner mit Familien (sammelt rekursiv alle .rfa-Dateien aus allen Unterordnern) und eine SPF-Datei (.txt).

Als Ausgabe erstellt das Tool, falls erforderlich, 3 Dateien in dem für die Familiendateien definierten Ordner:
- "FailedFamilies.txt" - Dateien, bei denen etwas schief gegangen ist
- "FamiliesOver2020.txt" - fehlgeschlagene Dateien, die in einer späteren Version von Revit gespeichert wurden
- SucceededFiles.csv" - Bericht mit Erfolgsstatus für jede Parameterersetzung (Erfolg, Misserfolg, für einige Fälle, warum fehlgeschlagen)

2. ReplaceSharedParameterNotFromSPFWithFamilyParameter
Ersetzt gemeinsam genutzte Parameter in einer Familiendatei, die nicht aus einer benutzerdefinierten SPF stammen (getrennt nach Name und Guid geprüft), durch Familienparameter mit demselben Namen.
Nur für Host(Top-Level)-Familien.
Behält Assoziationen mit Parametern von verschachtelten Familieninstanzen bei.
Behält Parameterwerte von Instanzen verschachtelter Familien innerhalb der Host-Familie bei.

Das Tool verlangt als Eingabe einen Ordner mit Familien (sammelt rekursiv alle .rfa-Dateien aus allen Unterordnern) und eine SPF-Datei (.txt).

Als Ausgabe erstellt das Tool, falls erforderlich, 3 Dateien in dem für die Familiendateien definierten Ordner:
- "FailedFamilies.txt" - Dateien, bei denen etwas schief gegangen ist
- "FamiliesOver2020.txt" - fehlgeschlagene Dateien, die in einer späteren Version von Revit gespeichert wurden
- SucceededFiles.csv" - Bericht mit Erfolgsstatus für jede Parameterersetzung (Erfolg, Misserfolg, für einige Fälle, warum fehlgeschlagen)

3. ChangeSharedParameterGUIDAccordingNameInSPF
Ersetzt gemeinsam genutzte Parameter, die vom Namen her mit Parametern im SPF übereinstimmen, aber nicht von der Guid her, durch Parameter mit der korrekten Parameter-GUID gemäß dem SPF.
Behält Assoziationen mit Parametern von verschachtelten Familieninstanzen bei und ersetzt gemeinsam genutzte Parameter in Beschriftungen der Familienkategorie Tag durch Workaround.
Behält Parameterwerte von Instanzen verschachtelter Familien innerhalb der Host-Familie bei.

Das Tool verlangt als Eingabe einen Ordner mit Familien (sammelt rekursiv alle .rfa-Dateien aus allen Unterordnern) und eine SPF-Datei (.txt).

Als Ausgabe erstellt das Tool, falls erforderlich, 3 Dateien in dem für die Familiendateien definierten Ordner:
- "FailedFamilies.txt" - Dateien, bei denen etwas schief gegangen ist
- "FamiliesOver2020.txt" - fehlgeschlagene Dateien, die in einer späteren Version von Revit gespeichert wurden
- SucceededFiles.csv" - Bericht mit Erfolgsstatus für jede Parameterersetzung (Erfolg, Misserfolg, für einige Fälle, warum fehlgeschlagen)

4. RenameFamilyParameters
Benennt Familienparameter in der Host-Familie (oberste Ebene) gemäß der .csv-Datei um.

Das Tool verlangt als Eingabe einen Ordner mit Familien (sammelt rekursiv alle .rfa-Dateien aus allen Unterordnern) und eine Datei mit Umbenennungsdaten (.csv)
im Format: 1't Spalte - aktueller Name, 2'd Spalte - neuer Name.

Als Ausgabe erstellt das Tool, falls erforderlich, 4 Dateien in dem für die Familiendateien definierten Ordner:
- "FailedFamilies.txt" - Dateien, bei denen etwas schief gegangen ist
- "FamiliesOver2020.txt" - fehlgeschlagene Dateien, die in einer späteren Version von Revit gespeichert wurden
- SucceededFiles.csv" - Bericht mit Erfolgsstatus für jede Parameterersetzung (Erfolg, Misserfolg, in einigen Fällen warum fehlgeschlagen)
- NamesNotFound.csv" - Bericht über die aktuellen Namen aus der csv-Datei, die in den Familiendateien nicht gefunden wurden

Additional tools:
1. FamilyRevitVersionReport
Erzeugt einen Bericht über die Einhaltung der Revit-Version (2020)

Das Tool verlangt als Eingabe einen Ordner mit Familien (sammelt rekursiv alle .rfa-Dateien aus allen Unterordnern).

Als Ausgabe erstellt das Tool eine csv-Datei in dem für die Familiendateien definierten Ordner:
- "RevVersionReport.csv"

2. NullNameTypesCleaner
Bereinigt Familien von Typen ohne Namen, wenn es mehr als einen Typ gibt.

Das Tool verlangt als Eingabe einen Ordner mit Familien (sammelt rekursiv alle .rfa-Dateien aus allen Unterordnern).

Als Ausgabe erstellt das Tool, wenn nötig, 2 Dateien in dem für Familiendateien definierten Ordner:
- "FailedFamilies.txt" - Dateien, bei denen etwas schief gegangen ist
- "FamiliesOver2020.txt" - fehlgeschlagene Dateien, die in einer späteren Version von Revit gespeichert wurden

3. RenameTypes
Benennt den Familientyp gemäß der .csv-Datei um.

Das Tool verlangt als Eingabe einen Ordner mit Familien (sammelt rekursiv alle .rfa-Dateien aus allen Unterordnern) und eine Datei mit Umbenennungsdaten (.csv)
im Format: 1't Spalte - Familienname, 2'd Spalte - aktueller Typname, 3'd Spalte - neuer Typname.

Als Ausgabe erstellt das Tool, falls erforderlich, 4 Dateien in dem für Familiendateien definierten Ordner:
- "FailedFamilies.txt" - Dateien, bei denen etwas schief gegangen ist
- "FamiliesOver2020.txt" - fehlgeschlagene Dateien, die in einer späteren Version von Revit gespeichert wurden
- SucceededFiles.csv" - Bericht mit Erfolgsstatus für jede Parameterersetzung (Erfolg, Misserfolg, in einigen Fällen warum fehlgeschlagen)
- FamiliesWithoutFile.txt" - Bericht über die Familiennamen aus den .csv-Daten, die nicht im Familienordner gefunden wurden
