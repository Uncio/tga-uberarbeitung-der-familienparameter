﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels
{
    public class ParameterValueData
    {
        public ElementId ParameterId { get; set; }
        public string Name { get; set; }
        public StorageType StorageType { get; set; }
        public object Value { get; set; }
        public Guid GUID { get; set; }
        public bool IsReadOnly { get; set; }

        public ParameterValueData(Parameter parameter)
        {
            Name = parameter.Definition.Name;
            ParameterId = parameter.Id;
            StorageType = parameter.StorageType;
            switch (StorageType)
            {
                case StorageType.Double:
                    Value = parameter.AsDouble();
                    break;
                case StorageType.Integer:
                    Value = parameter.AsInteger();
                    break;
                case StorageType.ElementId:
                    Value = parameter.AsElementId();
                    break;
                case StorageType.String:
                    Value = parameter.AsString();
                    break;
            }

            if (parameter.IsShared)
            {
                GUID = parameter.GUID;
            }

            IsReadOnly = parameter.IsReadOnly;
        }
    }
}
