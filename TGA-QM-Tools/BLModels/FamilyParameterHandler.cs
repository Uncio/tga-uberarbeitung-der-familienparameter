﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels
{
    public class FamilyParameterHandler
    {
        private readonly Document document;
        private FamilyManager famManager;

        public FamilyParameterHandler(Document doc)
        {
            document = doc;
            famManager = doc.FamilyManager;
        }

        public bool RenameParameter(FamilyParameter parameter, string newName)
        {
            try
            {
                famManager.RenameParameter(parameter, newName);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
