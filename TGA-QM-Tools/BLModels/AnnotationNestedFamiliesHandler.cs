﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.BLModels.Helpers;
using TGA_QM_Tools.BLModels.SPF_logic;

namespace TGA_QM_Tools.BLModels
{
    public class AnnotationNestedFamiliesHandler
    {
        private readonly Document document;
        private Dictionary<Guid, Guid> fakeGuidsDictionary = new Dictionary<Guid, Guid>();
        private Dictionary<Guid, string> fakeGuidsNameDictionary = new Dictionary<Guid, string>();
        private Dictionary<Guid, bool> parametersToDelete = new Dictionary<Guid, bool>();
        private List<Document> openedFamilies;
        private List<InstanceParameterData> nestedFamilyInstances;
        private List<ParameterAssociation> nestedFamiliesAssociations;

        public AnnotationNestedFamiliesHandler(Document doc)
        {
            document = doc;
        }

        public bool ReplaceSharedParametersWithFake(List<Guid> guidsToCheckInAnnotations,
                                                    out List<ParameterAssociation> familiesAssociations,
                                                    out bool nestedChanged,
                                                    out List<Guid> toDeleteInMain)
        {
            nestedChanged = false;
            toDeleteInMain = new List<Guid>();
            nestedFamilyInstances = new List<InstanceParameterData>();
            nestedFamiliesAssociations = new List<ParameterAssociation>();
            familiesAssociations = new List<ParameterAssociation>();

            var nestedAnnotationFamilies = GetAllNestedAnnotationFamilies();
            var associations = new Dictionary<FamilyParameter, List<Parameter>>();
            document.FamilyManager.Parameters.Cast<FamilyParameter>()
                .Where(x => !x.AssociatedParameters.IsEmpty).ToList()
                .ForEach(x => associations.Add(x, new List<Parameter>(x.AssociatedParameters.Cast<Parameter>())));

            openedFamilies = new List<Document>();

            foreach(var family in nestedAnnotationFamilies)
            {
                var famDoc = document.EditFamily(family);

                var famSPElements = new List<SharedParameterElement>();
                using (var col = new FilteredElementCollector(famDoc))
                {
                    famSPElements = col.OfClass(typeof(SharedParameterElement)).Cast<SharedParameterElement>().ToList();
                }
                    
                if (famSPElements.Any(x => guidsToCheckInAnnotations.Any(z => z == x.GuidValue)))
                {
                    using (var col = new FilteredElementCollector(document).OfClass(typeof(FamilyInstance)))
                    {
                        col.Where(x => (x as FamilyInstance).Symbol.Family.Id == family.Id).ToList()
                            .ForEach(x => nestedFamilyInstances.Add(new InstanceParameterData(x)));
                    }

                    openedFamilies.Add(famDoc);
                    var famParameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();

                    foreach (var spElement in famSPElements.Where(x => guidsToCheckInAnnotations.Any(z => z == x.GuidValue)))
                    {
                        nestedChanged = true;
                        if (associations.Any(x => x.Value.Any(z => z.IsShared && z.GUID == spElement.GuidValue || x.Key.IsShared && x.Key.GUID == spElement.GuidValue)))
                        {
                            associations.Where(x => x.Value.Any(z => z.IsShared && z.GUID == spElement.GuidValue)
                            || x.Key.IsShared && x.Key.GUID == spElement.GuidValue).ToList()
                                .ForEach(x => nestedFamiliesAssociations.Add(
                                new ParameterAssociation(
                                    x.Key,
                                    new List<Parameter> ( x.Value ))));
                        }

                        var oldGuid = spElement.GuidValue;
                        toDeleteInMain.Add(oldGuid);
                        var parameterName = spElement.Name;

                        var newParameter = famDoc.ReplaceSharedParameterWithFake(spElement, famParameters.ToList(), out string name, out bool toDelete);
                        parametersToDelete.Add(newParameter.GUID, toDelete);
                        if (newParameter != null)
                        {
                            fakeGuidsDictionary.Add(newParameter.GUID, oldGuid);
                            fakeGuidsNameDictionary.Add(newParameter.GUID, parameterName);
                        }
                        else
                        {
                            CloseOpenedFiles(openedFamilies);
                            return false;
                        }
                    }

                    try
                    {
                        var newFamily = famDoc.LoadFamily(document, new LoadOptions());
                    }
                    catch (Exception ex)
                    {
                        CloseOpenedFiles(openedFamilies);
                        return false;
                    }
                }
                else
                {
                    famDoc.Close(false);
                }
            }

            familiesAssociations = nestedFamiliesAssociations;

            return true;
        }

        private List<Family> GetAllNestedAnnotationFamilies()
        {
            var result = new List<Family>();
             
            using (var col = new FilteredElementCollector(document).OfClass(typeof(Family)))
            {
                result = col
                    .Where(x => /*(x as Family).FamilyCategory.CategoryType == CategoryType.Annotation &&*/ (x as Family).IsEditable)
                    .Cast<Family>().ToList();
            }

            return result;
        }

        public bool ReplaceDummyParametersWithRenamedParameter()
        {
            try
            {
                foreach (var famDoc in openedFamilies)
                {
                    var spHandler = new SharedParametersHandler(famDoc);
                    var spfReader = new ReadingSPF(document.Application);

                    var famParameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();

                    foreach (var parameter in famParameters)
                    {
                        if (parameter.IsShared && fakeGuidsDictionary.ContainsKey(parameter.GUID))
                        {
                            using (var tr = new Transaction(famDoc, "fake parameter"))
                            {
                                tr.Start();

                                var toDelete = parametersToDelete[parameter.GUID];
                                var extDef = spfReader.Definitions.FirstOrDefault(x => x.GUID == fakeGuidsDictionary[parameter.GUID]);
                                var newParameter = spHandler.ReplaceWithSharedParameterSameType(parameter, extDef);
                                if (famDoc.OwnerFamily.FamilyCategory.CategoryType == CategoryType.Annotation &&
                                    famDoc.OwnerFamily.FamilyCategory.Id.IntegerValue != (int)BuiltInCategory.OST_GenericAnnotation
                                    && toDelete)
                                    famDoc.FamilyManager.RemoveParameter(newParameter);

                                tr.Commit();
                            }
                        }
                    }

                    var newFamily = famDoc.LoadFamily(document, new LoadOptions());
                }

                using (var col = new FilteredElementCollector(document).OfClass(typeof(SharedParameterElement)))
                {
                    var spElementsToDelete = col.Where(x => fakeGuidsDictionary.Keys.Any(z => z == (x as SharedParameterElement).GuidValue));
                    using (var tr = new Transaction(document, "deleting fake parameters"))
                    {
                        tr.Start();
                        document.Delete(spElementsToDelete.Select(x => x.Id).ToList());
                        tr.Commit();
                    }
                }
            }
            catch
            {
                return false;
            }
            finally
            {
                CloseOpenedFiles(openedFamilies);
            }

            return true;
        }        

        public bool ReplaceDummyParametersWithRenamedParameterByName()
        {
            try
            {
                foreach (var famDoc in openedFamilies)
                {
                    var spHandler = new SharedParametersHandler(famDoc);
                    var spfReader = new ReadingSPF(document.Application);

                    var famParameters = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>();

                    foreach (var parameter in famParameters)
                    {
                        if (parameter.IsShared && fakeGuidsNameDictionary.ContainsKey(parameter.GUID))
                        {
                            using (var tr = new Transaction(famDoc, "fake parameter"))
                            {
                                tr.Start();

                                var toDelete = parametersToDelete[parameter.GUID];
                                var extDef = spfReader.Definitions.FirstOrDefault(x => x.Name == fakeGuidsNameDictionary[parameter.GUID]);
                                var newParameter = spHandler.ReplaceWithSharedParameterSameType(parameter, extDef);
                                if (famDoc.OwnerFamily.FamilyCategory.CategoryType == CategoryType.Annotation &&
                                    famDoc.OwnerFamily.FamilyCategory.Id.IntegerValue != (int)BuiltInCategory.OST_GenericAnnotation
                                    && toDelete)
                                    famDoc.FamilyManager.RemoveParameter(newParameter);

                                tr.Commit();
                            }
                        }
                    }

                    var newFamily = famDoc.LoadFamily(document, new LoadOptions());
                }

                using (var col = new FilteredElementCollector(document).OfClass(typeof(SharedParameterElement)))
                {
                    var spElementsToDelete = col.Where(x => fakeGuidsNameDictionary.Keys.Any(z => z == (x as SharedParameterElement).GuidValue));
                    using (var tr = new Transaction(document, "deleting fake parameters"))
                    {
                        tr.Start();
                        document.Delete(spElementsToDelete.Select(x => x.Id).ToList());
                        tr.Commit();
                    }
                }
            }
            catch
            {
                return false;
            }
            finally
            {
                CloseOpenedFiles(openedFamilies);
            }

            return true;
        }

        public bool RestoreAssociationsAndValues(bool byNameOnly = false)
        {
            try
            {
                foreach (var valueSet in nestedFamilyInstances)
                {
                    if (valueSet.ParametersValues.Count != 0)
                    {
                        var element = document.GetElement(valueSet.InstanceId);
                        var elementParameters = element.Parameters.Cast<Parameter>();

                        foreach (var parameter in valueSet.ParametersValues)
                        {
                            Parameter parameterElem = null;
                            if (parameter.GUID != null && parameter.GUID != new Guid() && !byNameOnly)
                            {
                                parameterElem = elementParameters.FirstOrDefault(x => x.IsShared && x.GUID == parameter.GUID);
                            }
                            else
                            {
                                parameterElem = elementParameters.FirstOrDefault(x => x.Definition.Name == parameter.Name);
                            }

                            if (!parameter.IsReadOnly)
                            //if (parameter.GUID != null && guidsToCheckInAnnotations.Any(x => parameter.GUID == x))
                            {
                                switch (parameter.StorageType)
                                {
                                    case StorageType.Double:
                                        parameterElem.Set((double)parameter.Value);
                                        break;
                                    case StorageType.Integer:
                                        parameterElem.Set((int)parameter.Value);
                                        break;
                                    case StorageType.ElementId:
                                        parameterElem.Set((ElementId)parameter.Value);
                                        break;
                                    case StorageType.String:
                                        parameterElem.Set((string)parameter.Value);
                                        break;
                                }
                            }
                        }
                    }
                }

                if (nestedFamiliesAssociations.Count != 0)
                {
                    var handler = new AssociationsHandler(document);

                    foreach (var association in nestedFamiliesAssociations)
                    {
                        bool success = false;
                        if (byNameOnly)
                        {
                            success = handler.ReassociateRenamedSharedParameterByName(association);
                        }
                        else
                        {
                            success = handler.ReassociateRenamedNestedSharedParameter(association);
                        }

                        if (!success)
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private void CloseOpenedFiles(List<Document> docs)
        {
            foreach (var doc in docs)
            {
                if (doc != null && doc.IsValidObject)
                {
                    doc.Close(false);
                }
            }
        }
    }
}
