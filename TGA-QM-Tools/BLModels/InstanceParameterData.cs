﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels
{
    public class InstanceParameterData
    {
        public ElementId InstanceId { get; set; }
        public List<ParameterValueData> ParametersValues { get; set; }

        public InstanceParameterData(Element instance)
        {
            InstanceId = instance.Id;
            var parameters = instance.Parameters;
            ParametersValues = new List<ParameterValueData>();
            foreach(Parameter parameter in parameters)
            {
                    ParametersValues.Add(new ParameterValueData(parameter));
            }
        }
    }
}
