﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels.Helpers
{
    internal class FiltersLibraryUtil
    {
        private List<Models.FilterData> existFilters;
        Document doc;
        IList<Element> existingFilters = null;
        public FiltersLibraryUtil(Document doc)
        {
            existFilters = new List<Models.FilterData>();
            this.doc = doc;
            existingFilters = new FilteredElementCollector(doc).OfClass(typeof(ParameterFilterElement)).ToElements();
        }

        public List<Models.FilterData> CreateFiltersLibrary()
        {
            foreach (var filter in existingFilters)
            {

                var filterParameters = new List<string>();
                var eFilter = new Models.FilterData();
                eFilter.FilterId = filter.Id.IntegerValue;
                eFilter.FilterName = filter.Name;
                var elFilter = (filter as ParameterFilterElement).GetElementFilter();
                eFilter.Rules = new Models.Rules();

                if (elFilter != null)
                {
                    if (elFilter.GetType() == typeof(LogicalAndFilter))
                    {

                        eFilter.Rules.AND = new List<Models.And>();

                        foreach (var parElementFilter in (elFilter as LogicalAndFilter).GetFilters())
                        {
                            var and1 = new Models.And();
                            if (parElementFilter.GetType() == typeof(ElementParameterFilter))
                            {
                                if ((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter().IntegerValue < 0)
                                {
                                }
                                else if (doc.GetElement((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) is SharedParameterElement)
                                {
                                    and1.Parameter = (doc.GetElement((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) as ParameterElement).Name;
                                    filterParameters.Add(and1.Parameter);
                                    if ((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault().GetType() != typeof(FilterInverseRule))
                                    {
                                        var typeValue = CreateFilterRuleType((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault());
                                        and1.Type = typeValue.Item1;
                                        and1.Value = typeValue.Item2.ToString();
                                        //and1.Value = ((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule).RuleString;
                                    }
                                    else
                                    {
                                        var typeValue = CreateFilterRuleType(((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule());
                                        and1.Type = "Not" + typeValue.Item1;
                                        and1.Value = typeValue.Item2.ToString();
                                        //and1.Type = "Not" + CreateFilterRuleType(((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule);
                                        //and1.Value = (((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule).RuleString;
                                    }
                                }
                            }
                            else if (parElementFilter.GetType() == typeof(LogicalOrFilter))
                            {
                                and1.OR = new List<Models.Or>();

                                foreach (var filt in (parElementFilter as ElementLogicalFilter).GetFilters())
                                {
                                    var or1 = new Models.Or();
                                    if (filt.GetType() == typeof(ElementParameterFilter))
                                    {
                                        if ((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter().IntegerValue < 0)
                                        {
                                        }
                                        else if(doc.GetElement((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) is SharedParameterElement)
                                        {
                                            or1.Parameter = (doc.GetElement((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) as ParameterElement).Name;
                                            filterParameters.Add(or1.Parameter);
                                            if ((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetType() != typeof(FilterInverseRule))
                                            {
                                                var typeValue = CreateFilterRuleType((filt as ElementParameterFilter).GetRules().FirstOrDefault());
                                                or1.Type = typeValue.Item1;
                                                or1.Value = typeValue.Item2.ToString();
                                                //or1.Type = CreateFilterRuleType((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule);
                                                //or1.Value = ((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule).RuleString;
                                            }
                                            else
                                            {
                                                var typeValue = CreateFilterRuleType(((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule());
                                                or1.Type = "Not" + typeValue.Item1;
                                                or1.Value = typeValue.Item2.ToString();
                                                //or1.Type = "Not" + CreateFilterRuleType(((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule);
                                                //or1.Value = (((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule).RuleString;
                                            }
                                        }
                                    }
                                    else if (filt.GetType() == typeof(LogicalAndFilter))
                                    {
                                        or1.AND = new List<Models.And>();
                                        foreach (var and2filt in (filt as ElementLogicalFilter).GetFilters())
                                        {
                                            var and2 = new Models.And();
                                            if ((and2filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter().IntegerValue < 0)
                                            {
                                            }
                                            else if (doc.GetElement((and2filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) is SharedParameterElement)
                                            {
                                                and2.Parameter = (doc.GetElement((and2filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) as ParameterElement).Name;
                                                filterParameters.Add(and2.Parameter);
                                                if ((and2filt as ElementParameterFilter).GetRules().FirstOrDefault().GetType() != typeof(FilterInverseRule))
                                                {
                                                    var typeValue = CreateFilterRuleType((and2filt as ElementParameterFilter).GetRules().FirstOrDefault());
                                                    and2.Type = typeValue.Item1;
                                                    and2.Value = typeValue.Item2.ToString();
                                                    //and2.Type = CreateFilterRuleType((and2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule);
                                                    //and2.Value = ((and2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule).RuleString;
                                                }
                                                else
                                                {
                                                    var typeValue = CreateFilterRuleType(((and2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule());
                                                    and2.Type = "Not" + typeValue.Item1;
                                                    and2.Value = typeValue.Item2.ToString();
                                                    //and2.Type = "Not" + CreateFilterRuleType(((and2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule);
                                                    //and2.Value = (((and2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule).RuleString;
                                                }
                                            }
                                            or1.AND.Add(and2);
                                        }

                                    }
                                    else if (filt.GetType() == typeof(LogicalOrFilter))
                                    {
                                        or1.OR = new List<Models.Or>();
                                        foreach (var or2filt in (filt as ElementLogicalFilter).GetFilters())
                                        {
                                            var or3 = new Models.Or();
                                            if ((or2filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter().IntegerValue < 0)
                                            {
                                            }
                                            else if (doc.GetElement((or2filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) is SharedParameterElement)
                                            {
                                                or3.Parameter = (doc.GetElement((or2filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) as ParameterElement).Name;
                                                filterParameters.Add(or3.Parameter);
                                                if ((or2filt as ElementParameterFilter).GetRules().FirstOrDefault().GetType() != typeof(FilterInverseRule))
                                                {
                                                    var typeValue = CreateFilterRuleType((or2filt as ElementParameterFilter).GetRules().FirstOrDefault());
                                                    or3.Type = typeValue.Item1;
                                                    or3.Value = typeValue.Item2.ToString();
                                                    //or3.Type = CreateFilterRuleType((or2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule);
                                                    //or3.Value = ((or2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule).RuleString;
                                                }
                                                else
                                                {
                                                    var typeValue = CreateFilterRuleType(((or2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule());
                                                    or3.Type = "Not" + typeValue.Item1;
                                                    or3.Value = typeValue.Item2.ToString();
                                                    //or3.Type = "Not" + CreateFilterRuleType(((or2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule);
                                                    //or3.Value = (((or2filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule).RuleString;
                                                }
                                            }
                                            or1.OR.Add(or3);
                                        }
                                    }
                                    and1.OR.Add(or1);
                                }
                            }
                            else if (parElementFilter.GetType() == typeof(LogicalAndFilter))
                            {
                                and1.AND = new List<Models.And>();
                                foreach (var filt in (parElementFilter as ElementLogicalFilter).GetFilters())
                                {
                                    var and2 = new Models.And();
                                    if ((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter().IntegerValue < 0)
                                    {
                                    }
                                    else if(doc.GetElement((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) is SharedParameterElement)
                                    {
                                        and2.Parameter = (doc.GetElement((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) as ParameterElement).Name;
                                        filterParameters.Add(and2.Parameter);
                                        if ((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetType() != typeof(FilterInverseRule))
                                        {
                                            var typeValue = CreateFilterRuleType((filt as ElementParameterFilter).GetRules().FirstOrDefault());
                                            and2.Type = typeValue.Item1;
                                            and2.Value = typeValue.Item2.ToString();
                                            //and2.Type = CreateFilterRuleType((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule);
                                            //and2.Value = ((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule).RuleString;
                                        }
                                        else
                                        {
                                            var typeValue = CreateFilterRuleType(((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule());
                                            and2.Type = "Not" + typeValue.Item1;
                                            and2.Value = typeValue.Item2.ToString();
                                            //and2.Type = "Not" + CreateFilterRuleType(((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule);
                                            //and2.Value = (((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule).RuleString;
                                        }
                                    }
                                    and1.AND.Add(and2);
                                }
                            }
                            eFilter.Rules.AND.Add(and1);
                        }
                    }
                    else
                    {

                        eFilter.Rules.OR = new List<Models.Or>();
                        //var test = 
                        foreach (var andFilter in (elFilter as LogicalOrFilter).GetFilters())
                        {
                            var or11 = new Models.Or();
                            if (andFilter.GetType() == typeof(ElementParameterFilter))
                            {
                                if ((andFilter as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter().IntegerValue < 0)
                                {
                                }
                                else if (doc.GetElement((andFilter as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) is SharedParameterElement)
                                {
                                    or11.Parameter = (doc.GetElement((andFilter as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) as ParameterElement).Name;
                                    filterParameters.Add(or11.Parameter);
                                    if ((andFilter as ElementParameterFilter).GetRules().FirstOrDefault().GetType() != typeof(FilterInverseRule))
                                    {
                                        var typeValue = CreateFilterRuleType((andFilter as ElementParameterFilter).GetRules().FirstOrDefault());
                                        or11.Type = typeValue.Item1;
                                        or11.Value = typeValue.Item2.ToString();
                                        //or11.Type = CreateFilterRuleType((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule);
                                        //or11.Value = ((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule).RuleString;
                                    }
                                    else
                                    {
                                        var typeValue = CreateFilterRuleType(((andFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule());
                                        or11.Type = "Not" + typeValue.Item1;
                                        or11.Value = typeValue.Item2.ToString();
                                        //or11.Type = "Not" + CreateFilterRuleType(((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule);
                                        //or11.Value = (((parElementFilter as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule).RuleString;
                                    }
                                }
                            }
                            else if (andFilter.GetType() == typeof(LogicalOrFilter))
                            {
                                or11.OR = new List<Models.Or>();
                                foreach (var filt in (andFilter as ElementLogicalFilter).GetFilters())
                                {
                                    var or1 = new Models.Or();
                                    if ((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter().IntegerValue < 0)
                                    {
                                    }
                                    else if (doc.GetElement((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) is SharedParameterElement)
                                    {
                                        or1.Parameter = (doc.GetElement((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) as ParameterElement).Name;
                                        filterParameters.Add(or1.Parameter);
                                        if ((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetType() != typeof(FilterInverseRule))
                                        {
                                            var typeValue = CreateFilterRuleType((filt as ElementParameterFilter).GetRules().FirstOrDefault());
                                            or1.Type = typeValue.Item1;
                                            or1.Value = typeValue.Item2.ToString();
                                            //or1.Type = CreateFilterRuleType((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule);
                                            //or1.Value = ((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule).RuleString;
                                        }
                                        else
                                        {
                                            var typeValue = CreateFilterRuleType(((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule());
                                            or1.Type = "Not" + typeValue.Item1;
                                            or1.Value = typeValue.Item2.ToString();
                                            //or1.Type = "Not" + CreateFilterRuleType(((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule);
                                            // or1.Value = (((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule).RuleString;
                                        }
                                    }
                                    or11.OR.Add(or1);
                                }
                            }
                            else if (andFilter.GetType() == typeof(LogicalAndFilter))
                            {
                                or11.AND = new List<Models.And>();
                                foreach (var filt in (andFilter as ElementLogicalFilter).GetFilters())
                                {
                                    var and2 = new Models.And();
                                    if ((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter().IntegerValue < 0)
                                    {
                                    }
                                    else if (doc.GetElement((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) is SharedParameterElement)
                                    {
                                        and2.Parameter = (doc.GetElement((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetRuleParameter()) as ParameterElement).Name;
                                        filterParameters.Add(and2.Parameter);
                                        if ((filt as ElementParameterFilter).GetRules().FirstOrDefault().GetType() != typeof(FilterInverseRule))
                                        {
                                            var typeValue = CreateFilterRuleType((filt as ElementParameterFilter).GetRules().FirstOrDefault());
                                            and2.Type = typeValue.Item1;
                                            and2.Value = typeValue.Item2.ToString();
                                            //and2.Type = CreateFilterRuleType((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule);
                                            //and2.Value = ((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterStringRule).RuleString;
                                        }
                                        else
                                        {
                                            var typeValue = CreateFilterRuleType(((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule());
                                            and2.Type = "Not" + typeValue.Item1;
                                            and2.Value = typeValue.Item2.ToString();
                                            //and2.Type = "Not" + CreateFilterRuleType(((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule);
                                            //and2.Value = (((filt as ElementParameterFilter).GetRules().FirstOrDefault() as FilterInverseRule).GetInnerRule() as FilterStringRule).RuleString;
                                        }
                                    }
                                    or11.AND.Add(and2);
                                }
                            }
                            eFilter.Rules.OR.Add(or11);
                        }
                    }

                }
                eFilter.Parameters.AddRange(filterParameters.Distinct().ToList());
                existFilters.Add(eFilter);
            }

            return existFilters;
        }

        public Dictionary<string, List<Models.FilterData>> CreateViewsLibrary(List<Models.FilterData> filtersData)
        {
            var viewsDic = new Dictionary<string, List<Models.FilterData>>();
            var views = new FilteredElementCollector(doc).OfClass(typeof(View)).ToElements();
            foreach (View view in views)
            {
                if (view.AreGraphicsOverridesAllowed())
                {
                    var viewFilters = view.GetFilters();
                    if (viewFilters.Count != 0)
                    {
                        viewsDic.Add(view.Name, new List<Models.FilterData>());
                        foreach (ElementId filter in viewFilters)
                        {
                            var currentFilter = filtersData.FirstOrDefault(x => x.FilterId == filter.IntegerValue);
                            viewsDic[view.Name].Add(currentFilter);
                        }
                    }
                }
            }

            return viewsDic;
        }

        public void ReplaceFiltersWithNewParameter(string newParameterName, string parameterName, List<Models.FilterData> filters)
        {
            ElementId paramid = new FilteredElementCollector(doc).OfClass(typeof(SharedParameterElement)).
                FirstOrDefault(x => (x as SharedParameterElement).Name == newParameterName).Id;
            try
            {
                foreach (var filter in filters)
                {
                    ParameterFilterElement filterElem = new FilteredElementCollector(doc).OfClass(typeof(ParameterFilterElement)).
                        FirstOrDefault(x => x.Id.IntegerValue == filter.FilterId) as ParameterFilterElement;
                    var elFilter = filterElem.GetElementFilter();
                    if (elFilter != null)
                    {
                        if (elFilter.GetType() == typeof(LogicalAndFilter))
                        {
                            var allNewFilters = new List<ElementFilter>();
                            //and filters
                            var allAndFilters = (elFilter as LogicalAndFilter).GetFilters().Where(x => x is LogicalAndFilter);
                            var allAndData = filter.Rules.AND.Where(x => x.AND != null).ToList();
                            //or filters
                            var allOrFilters = (elFilter as LogicalAndFilter).GetFilters().Where(x => x is LogicalOrFilter);
                            var allOrData = filter.Rules.AND.Where(x => x.OR != null).ToList();
                            //value filters
                            var allElemParamFilters = (elFilter as LogicalAndFilter).GetFilters().Where(x => x is ElementParameterFilter);
                            var allElemParamData = filter.Rules.AND.Where(x => x.AND == null && x.OR == null).ToList();
                            allNewFilters.AddRange(allElemParamFilters);

                            if (allAndFilters.Count() != 0)
                            {
                                foreach (var andFilter in allAndFilters)
                                {
                                    var andIndex = allAndFilters.ToList().IndexOf(andFilter);
                                    var andFilterData = allAndData[andIndex];

                                    var newFilters = new List<ElementFilter>();
                                    //and filters
                                    var andAndFilters = (andFilter as LogicalAndFilter).GetFilters().Where(x => x is LogicalAndFilter);
                                    var andAndData = andFilterData.AND.Where(x => x.AND != null).ToList();
                                    //or filters
                                    var andOrFilters = (andFilter as LogicalAndFilter).GetFilters().Where(x => x is LogicalOrFilter);
                                    var andOrData = andFilterData.AND.Where(x => x.OR != null).ToList();
                                    //value filters
                                    var andElemParamFilters = (andFilter as LogicalAndFilter).GetFilters().Where(x => x is ElementParameterFilter);
                                    var andElemParamData = andFilterData.AND.Where(x => x.AND == null && x.OR == null).ToList();
                                    newFilters.AddRange(andElemParamFilters);

                                    if (andAndFilters.Count() != 0)
                                    {
                                        foreach (var andAndFilterData in andAndData)
                                        {
                                            foreach (var item in andAndFilterData.AND)
                                            {
                                                var andAndIndex = andAndData.IndexOf(andAndFilterData);
                                                if (item.Parameter != null && item.Parameter == parameterName)
                                                {
                                                    var currentAndAndFilter = andAndFilters.ToList()[andAndIndex];
                                                    var currentFilters = (currentAndAndFilter as LogicalAndFilter).GetFilters();
                                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, item.Type, item.Value);
                                                    currentFilters.Add(newParameterFilter);
                                                    var newFilter = (currentAndAndFilter as ElementLogicalFilter);
                                                    newFilter.SetFilters(currentFilters);
                                                    newFilters.Add(newFilter);
                                                }
                                                else
                                                    newFilters.Add(andAndFilters.ToList()[andAndIndex]);
                                            }
                                        }
                                    }
                                    if (andOrFilters.Count() != 0)
                                    {
                                        foreach (var andOrFilterData in andOrData)
                                        {
                                            foreach (var item in andOrFilterData.OR)
                                            {
                                                var andOrIndex = andOrData.IndexOf(andOrFilterData);
                                                if (item.Parameter != null && item.Parameter == parameterName)
                                                {
                                                    var currentAndOrFilter = andOrFilters.ToList()[andOrIndex];
                                                    var currentFilters = (currentAndOrFilter as LogicalOrFilter).GetFilters();
                                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, item.Type, item.Value);
                                                    currentFilters.Add(newParameterFilter);
                                                    var newFilter = (currentAndOrFilter as ElementLogicalFilter);
                                                    newFilter.SetFilters(currentFilters);
                                                    newFilters.Add(newFilter);
                                                }
                                                else
                                                    newFilters.Add(andAndFilters.ToList()[andOrIndex]);
                                            }
                                        }
                                    }
                                    if (andElemParamFilters.Count() != 0)
                                    {
                                        foreach (var andElemParFilterData in andElemParamData)
                                        {
                                            var currentIndex = andElemParamData.IndexOf(andElemParFilterData);
                                            if (andElemParFilterData.Parameter != null && andElemParFilterData.Parameter == parameterName)
                                            {
                                                ElementFilter newParameterFilter = CreateNewFilter(paramid, andElemParFilterData.Type, andElemParFilterData.Value);
                                                newFilters.Add(newParameterFilter);
                                            }

                                        }
                                    }

                                    (andFilter as LogicalAndFilter).SetFilters(newFilters);
                                    allNewFilters.Add(andFilter);
                                }
                            }
                            if (allOrFilters.Count() != 0)
                            {
                                foreach (var orFilter in allOrFilters)
                                {
                                    var orIndex = allOrFilters.ToList().IndexOf(orFilter);
                                    var orFilterData = allOrData[orIndex];

                                    var newFilters = new List<ElementFilter>();
                                    //and filters
                                    var orAndFilters = (orFilter as LogicalOrFilter).GetFilters().Where(x => x is LogicalAndFilter);
                                    var orAndData = orFilterData.OR.Where(x => x.AND != null).ToList();
                                    //or filters
                                    var orOrFilters = (orFilter as LogicalOrFilter).GetFilters().Where(x => x is LogicalOrFilter);
                                    var orOrData = orFilterData.OR.Where(x => x.OR != null).ToList();
                                    //value filters
                                    var orElemParamFilters = (orFilter as LogicalOrFilter).GetFilters().Where(x => x is ElementParameterFilter);
                                    var orElemParamData = orFilterData.OR.Where(x => x.AND == null && x.OR == null).ToList();
                                    newFilters.AddRange(orElemParamFilters);

                                    if (orAndFilters.Count() != 0)
                                    {
                                        foreach (var orAndFilterData in orAndData)
                                        {
                                            foreach (var item in orAndFilterData.AND)
                                            {
                                                var orAndIndex = orAndData.IndexOf(orAndFilterData);
                                                if (item.Parameter != null && item.Parameter == parameterName)
                                                {
                                                    var currentOrAndFilter = orAndFilters.ToList()[orAndIndex];
                                                    var currentFilters = (currentOrAndFilter as LogicalAndFilter).GetFilters();
                                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, item.Type, item.Value);
                                                    currentFilters.Add(newParameterFilter);
                                                    //(currentOrAndFilter as ElementLogicalFilter).SetFilters(currentFilters);
                                                    var newFilter = (currentOrAndFilter as ElementLogicalFilter);
                                                    newFilter.SetFilters(currentFilters);
                                                    newFilters.Add(newFilter);
                                                }
                                                else
                                                    newFilters.Add(orAndFilters.ToList()[orAndIndex]);
                                            }
                                        }
                                    }
                                    if (orOrFilters.Count() != 0)
                                    {
                                        foreach (var orOrFilterData in orOrData)
                                        {
                                            foreach (var item in orOrFilterData.OR)
                                            {
                                                var orOrIndex = orOrData.IndexOf(orOrFilterData);
                                                if (item.Parameter != null && item.Parameter == parameterName)
                                                {
                                                    var currentOrOrFilter = orOrFilters.ToList()[orOrIndex];
                                                    var currentFilters = (currentOrOrFilter as LogicalOrFilter).GetFilters();
                                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, item.Type, item.Value);
                                                    currentFilters.Add(newParameterFilter);
                                                    //(currentOrOrFilter as ElementLogicalFilter).SetFilters(currentFilters);
                                                    var newFilter = (currentOrOrFilter as ElementLogicalFilter);
                                                    newFilter.SetFilters(currentFilters);
                                                    newFilters.Add(newFilter);
                                                }
                                                else
                                                    newFilters.Add(orAndFilters.ToList()[orOrIndex]);
                                            }
                                        }
                                    }
                                    if (orElemParamFilters.Count() != 0)
                                    {
                                        foreach (var orElemParFilterData in orElemParamData)
                                        {
                                            if (orElemParFilterData.Parameter != null && orElemParFilterData.Parameter == parameterName)
                                            {
                                                var newParameterFilter = CreateNewFilter(paramid, orElemParFilterData.Type, orElemParFilterData.Value) as ElementFilter;
                                                newFilters.Add(newParameterFilter);
                                            }
                                        }
                                    }

                                    (orFilter as LogicalOrFilter).SetFilters(newFilters);
                                    allNewFilters.Add(orFilter);
                                }
                            }
                            if (allElemParamFilters.Count() != 0)
                            {
                                foreach (var allElemParFilterData in allElemParamData)
                                {
                                    if (allElemParFilterData.Parameter != null && allElemParFilterData.Parameter == parameterName)
                                    {
                                        ElementFilter newParameterFilter = CreateNewFilter(paramid, allElemParFilterData.Type, allElemParFilterData.Value);
                                        allNewFilters.Add(newParameterFilter);
                                    }
                                }
                            }
                            ElementFilter finalFilter = filterElem.GetElementFilter();
                            (finalFilter as LogicalAndFilter).SetFilters(allNewFilters);
                            filterElem.SetElementFilter(finalFilter);
                        }
                        else
                        {
                            var allNewFilters = new List<ElementFilter>();
                            //and filters
                            var allAndFilters = (elFilter as LogicalOrFilter).GetFilters().Where(x => x is LogicalAndFilter);
                            var allAndData = filter.Rules.OR.Where(x => x.AND != null).ToList();
                            //or filters
                            var allOrFilters = (elFilter as LogicalOrFilter).GetFilters().Where(x => x is LogicalOrFilter);
                            var allOrData = filter.Rules.OR.Where(x => x.OR != null).ToList();
                            //value filters
                            var allElemParamFilters = (elFilter as LogicalOrFilter).GetFilters().Where(x => x is ElementParameterFilter);
                            var allElemParamData = filter.Rules.OR.Where(x => x.AND == null && x.OR == null).ToList();
                            allNewFilters.AddRange(allElemParamFilters);

                            if (allAndFilters.Count() != 0)
                            {
                                foreach (var andFilter in allAndFilters)
                                {
                                    var andIndex = allAndFilters.ToList().IndexOf(andFilter);
                                    var andFilterData = allAndData[andIndex];

                                    var newFilters = new List<ElementFilter>();
                                    //and filters
                                    var andAndFilters = (andFilter as LogicalAndFilter).GetFilters().Where(x => x is LogicalAndFilter);
                                    var andAndData = andFilterData.AND.Where(x => x.AND != null).ToList();
                                    //or filters
                                    var andOrFilters = (andFilter as LogicalAndFilter).GetFilters().Where(x => x is LogicalOrFilter);
                                    var andOrData = andFilterData.AND.Where(x => x.OR != null).ToList();
                                    //value filters
                                    var andElemParamFilters = (andFilter as LogicalAndFilter).GetFilters().Where(x => x is ElementParameterFilter);
                                    var andElemParamData = andFilterData.AND.Where(x => x.AND == null && x.OR == null).ToList();
                                    newFilters.AddRange(andElemParamFilters);

                                    if (andAndFilters.Count() != 0)
                                    {
                                        foreach (var andAndFilterData in andAndData)
                                        {
                                            foreach (var item in andAndFilterData.AND)
                                            {
                                                var andAndIndex = andAndData.IndexOf(andAndFilterData);
                                                if (item.Parameter != null && item.Parameter == parameterName)
                                                {
                                                    var currentAndAndFilter = andAndFilters.ToList()[andAndIndex];
                                                    var currentFilters = (currentAndAndFilter as LogicalAndFilter).GetFilters();
                                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, item.Type, item.Value);
                                                    currentFilters.Add(newParameterFilter);
                                                    var newFilter = (currentAndAndFilter as ElementLogicalFilter);
                                                    newFilter.SetFilters(currentFilters);
                                                    newFilters.Add(newFilter);
                                                }
                                                else
                                                    newFilters.Add(andAndFilters.ToList()[andAndIndex]);
                                            }
                                        }
                                    }
                                    if (andOrFilters.Count() != 0)
                                    {
                                        foreach (var andOrFilterData in andOrData)
                                        {
                                            foreach (var item in andOrFilterData.OR)
                                            {
                                                var andOrIndex = andOrData.IndexOf(andOrFilterData);
                                                if (item.Parameter != null && item.Parameter == parameterName)
                                                {
                                                    var currentAndOrFilter = andOrFilters.ToList()[andOrIndex];
                                                    var currentFilters = (currentAndOrFilter as LogicalOrFilter).GetFilters();
                                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, item.Type, item.Value);
                                                    currentFilters.Add(newParameterFilter);
                                                    var newFilter = (currentAndOrFilter as ElementLogicalFilter);
                                                    newFilter.SetFilters(currentFilters);
                                                    newFilters.Add(newFilter);
                                                }
                                                else
                                                    newFilters.Add(andOrFilters.ToList()[andOrIndex]);
                                            }
                                        }
                                    }
                                    if (andElemParamFilters.Count() != 0)
                                    {
                                        foreach (var andElemParFilterData in andElemParamData)
                                        {
                                            if (andElemParFilterData.Parameter != null && andElemParFilterData.Parameter == parameterName)
                                            {
                                                ElementFilter newParameterFilter = CreateNewFilter(paramid, andElemParFilterData.Type, andElemParFilterData.Value);
                                                newFilters.Add(newParameterFilter);
                                            }
                                        }
                                    }

                                    (andFilter as LogicalAndFilter).SetFilters(newFilters);
                                    allNewFilters.Add(andFilter);
                                }
                            }
                            if (allOrFilters.Count() != 0)
                            {
                                foreach (var orFilter in allOrFilters)
                                {
                                    var orIndex = allOrFilters.ToList().IndexOf(orFilter);
                                    var orFilterData = allOrData[orIndex];

                                    var newFilters = new List<ElementFilter>();
                                    //and filters
                                    var orAndFilters = (orFilter as LogicalOrFilter).GetFilters().Where(x => x is LogicalAndFilter);
                                    var orAndData = orFilterData.OR.Where(x => x.AND != null).ToList();
                                    //or filters
                                    var orOrFilters = (orFilter as LogicalOrFilter).GetFilters().Where(x => x is LogicalOrFilter);
                                    var orOrData = orFilterData.OR.Where(x => x.OR != null).ToList();
                                    //value filters
                                    var orElemParamFilters = (orFilter as LogicalOrFilter).GetFilters().Where(x => x is ElementParameterFilter);
                                    var orElemParamData = orFilterData.OR.Where(x => x.AND == null && x.OR == null).ToList();
                                    newFilters.AddRange(orElemParamFilters);

                                    if (orAndFilters.Count() != 0)
                                    {
                                        foreach (var orAndFilterData in orAndData)
                                        {
                                            foreach (var item in orAndFilterData.AND)
                                            {
                                                var orAndIndex = orAndData.IndexOf(orAndFilterData);
                                                if (item.Parameter != null && item.Parameter == parameterName)
                                                {
                                                    var currentOrAndFilter = orAndFilters.ToList()[orAndIndex];
                                                    var currentFilters = (currentOrAndFilter as LogicalAndFilter).GetFilters();
                                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, item.Type, item.Value);
                                                    currentFilters.Add(newParameterFilter);
                                                    var newFilter = (currentOrAndFilter as ElementLogicalFilter);
                                                    newFilter.SetFilters(currentFilters);
                                                    newFilters.Add(newFilter);
                                                }
                                                else
                                                    newFilters.Add(orAndFilters.ToList()[orAndIndex]);
                                            }
                                        }
                                    }
                                    if (orOrFilters.Count() != 0)
                                    {
                                        foreach (var orOrFilterData in orOrData)
                                        {
                                            foreach (var item in orOrFilterData.OR)
                                            {
                                                var orOrIndex = orOrData.IndexOf(orOrFilterData);
                                                if (item.Parameter != null && item.Parameter == parameterName)
                                                {
                                                    var currentOrOrFilter = orOrFilters.ToList()[orOrIndex];
                                                    var currentFilters = (currentOrOrFilter as LogicalOrFilter).GetFilters();
                                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, item.Type, item.Value);
                                                    currentFilters.Add(newParameterFilter);
                                                    var newFilter = (currentOrOrFilter as ElementLogicalFilter);
                                                    newFilter.SetFilters(currentFilters);
                                                    newFilters.Add(newFilter);
                                                }
                                                else
                                                    newFilters.Add(orOrFilters.ToList()[orOrIndex]);
                                            }
                                        }
                                    }
                                    if (orElemParamFilters.Count() != 0)
                                    {
                                        foreach (var orElemParFilterData in orElemParamData)
                                        {
                                            if (orElemParFilterData.Parameter != null && orElemParFilterData.Parameter == parameterName)
                                            {
                                                ElementFilter newParameterFilter = CreateNewFilter(paramid, orElemParFilterData.Type, orElemParFilterData.Value);
                                                newFilters.Add(newParameterFilter);
                                            }
                                        }
                                    }

                                    (orFilter as LogicalOrFilter).SetFilters(newFilters);
                                    allNewFilters.Add(orFilter);
                                }
                            }
                            if (allElemParamFilters.Count() != 0)
                            {
                                foreach (var allElemParFilterData in allElemParamData)
                                {
                                    if (allElemParFilterData.Parameter != null && allElemParFilterData.Parameter == parameterName)
                                    {
                                        ElementFilter newParameterFilter = CreateNewFilter(paramid, allElemParFilterData.Type, allElemParFilterData.Value);
                                        allNewFilters.Add(newParameterFilter);
                                    }
                                }
                            }

                            ElementFilter finalFilter = filterElem.GetElementFilter();
                            (finalFilter as LogicalOrFilter).SetFilters(allNewFilters);
                            filterElem.SetElementFilter(finalFilter);
                        }
                    }
                    else
                    {
                        var allNewFilters = new List<ElementFilter>();
                        ElementFilter finalFilter = null;
                        if (filter.Rules.AND != null)
                        {
                            var allElemParamData = filter.Rules.AND.Where(x => x.AND == null && x.OR == null).ToList();

                            foreach (var allElemParFilterData in allElemParamData)
                            {
                                if (allElemParFilterData.Parameter != null && allElemParFilterData.Parameter == parameterName)
                                {
                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, allElemParFilterData.Type, allElemParFilterData.Value);
                                    allNewFilters.Add(newParameterFilter);
                                }
                            }
                            finalFilter = new LogicalAndFilter(allNewFilters);
                            (finalFilter as LogicalAndFilter).SetFilters(allNewFilters);
                        }
                        else
                        {
                            var allElemParamData = filter.Rules.OR.Where(x => x.AND == null && x.OR == null).ToList();

                            foreach (var allElemParFilterData in allElemParamData)
                            {
                                if (allElemParFilterData.Parameter != null && allElemParFilterData.Parameter == parameterName)
                                {
                                    ElementFilter newParameterFilter = CreateNewFilter(paramid, allElemParFilterData.Type, allElemParFilterData.Value);
                                    allNewFilters.Add(newParameterFilter);
                                }
                            }
                            finalFilter = new LogicalOrFilter(allNewFilters);
                            (finalFilter as LogicalOrFilter).SetFilters(allNewFilters);
                        }

                        filterElem.SetElementFilter(finalFilter);
                    }
                }
            }
            catch (Exception ex)
            {
                var t = ex.Message;
            }



        }
        private (string, object) CreateFilterRuleType(FilterRule filterRule)
        {
            string ruleType = "";
            if (filterRule is FilterStringRule)
            {
                switch ((filterRule as FilterStringRule).GetEvaluator().ToString())
                {
                    case "Autodesk.Revit.DB.FilterStringEquals":
                        ruleType = "Equals";
                        break;
                    case "Autodesk.Revit.DB.FilterStringEndsWith":
                        ruleType = "EndsWith";
                        break;
                    case "Autodesk.Revit.DB.FilterStringBeginsWith":
                        ruleType = "BeginsWith";
                        break;
                    case "Autodesk.Revit.DB.FilterStringContains":
                        ruleType = "Contains";
                        break;
                    case "Autodesk.Revit.DB.FilterStringGreater":
                        ruleType = "Greater";
                        break;
                    case "Autodesk.Revit.DB.FilterStringGreaterOrEqual":
                        ruleType = "GreaterOrEqual";
                        break;
                    case "Autodesk.Revit.DB.FilterStringLess":
                        ruleType = "Less";
                        break;
                    case "Autodesk.Revit.DB.FilterStringLessOrEqual":
                        ruleType = "LessOrEqual";
                        break;
                }

                var value = "String_" + (filterRule as FilterStringRule).RuleString;
                return (ruleType, value);
            }

            if (filterRule is FilterNumericValueRule)
            {
                switch ((filterRule as FilterNumericValueRule).GetEvaluator().ToString())
                {
                    case "Autodesk.Revit.DB.FilterNumericEquals":
                        ruleType = "Equals";
                        break;
                    case "Autodesk.Revit.DB.FilterNumericGreater":
                        ruleType = "Greater";
                        break;
                    case "Autodesk.Revit.DB.FilterNumericGreaterOrEqual":
                        ruleType = "GreaterOrEqual";
                        break;
                    case "Autodesk.Revit.DB.FilterNumericLess":
                        ruleType = "Less";
                        break;
                    case "Autodesk.Revit.DB.FilterNumericLessOrEqual":
                        ruleType = "LessOrEqual";
                        break;
                }
                string value = String.Empty;
                if (filterRule is FilterDoubleRule)
                    value = "Double_" + (filterRule as FilterDoubleRule).RuleValue.ToString() + "_" + (filterRule as FilterDoubleRule).Epsilon.ToString();
                if (filterRule is FilterElementIdRule)
                    value = "ElementId_" + (filterRule as FilterElementIdRule).RuleValue.ToString();
                if (filterRule is FilterIntegerRule)
                    value = "Integer_" + (filterRule as FilterIntegerRule).RuleValue.ToString();
                return (ruleType, value);
            }

            return ("", null);
        }
        private FilterRule CreateFilterRule(ElementId paramId, string ruleType, string valueStr)
        {
            FilterRule parameterFilterElement = null;
            var value = valueStr.Remove(0, valueStr.IndexOf("_") + 1);
            switch (valueStr.Remove(valueStr.IndexOf("_")))
            {
                case "String":
                    switch (ruleType)
                    {
                        case "Equals":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateEqualsRule(paramId, value, true);
                            break;
                        case "NotEquals":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotEqualsRule(paramId, value, true);
                            break;
                        case "EndsWith":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateEndsWithRule(paramId, value, true);
                            break;
                        case "NotEndsWith":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotEndsWithRule(paramId, value, true);
                            break;
                        case "BeginsWith":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateBeginsWithRule(paramId, value, true);
                            break;
                        case "Contains":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateContainsRule(paramId, value, true);
                            break;
                        case "NotContains":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotContainsRule(paramId, value, true);
                            break;
                        case "Greater":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateGreaterRule(paramId, value, true);
                            break;
                        case "NotGreater":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotEndsWithRule(paramId, value, true);
                            break;
                        case "GreaterOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateBeginsWithRule(paramId, value, true);
                            break;
                        case "NotGreaterOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateContainsRule(paramId, value, true);
                            break;
                        case "Less":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotContainsRule(paramId, value, true);
                            break;
                        case "NotLess":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotContainsRule(paramId, value, true);
                            break;
                        case "LessOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotContainsRule(paramId, value, true);
                            break;
                        case "NotLessOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotContainsRule(paramId, value, true);
                            break;
                    }
                    break;
                case "Double":
                    var epsilon = value.Remove(0, value.IndexOf("_") + 1);
                    value = value.Remove(value.IndexOf("_"));

                    switch (ruleType)
                    {
                        case "Equals":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateEqualsRule(paramId, Double.Parse(value), Double.Parse(epsilon));
                            break;
                        case "NotEquals":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotEqualsRule(paramId, Double.Parse(value), Double.Parse(epsilon));
                            break;
                        case "Greater":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateGreaterRule(paramId, Double.Parse(value), Double.Parse(epsilon));
                            break;
                        case "NotGreater":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateGreaterOrEqualRule(paramId, Double.Parse(value), Double.Parse(epsilon));
                            break;
                        case "GreaterOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateLessRule(paramId, Double.Parse(value), Double.Parse(epsilon));
                            break;
                        case "NotGreaterOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateLessOrEqualRule(paramId, Double.Parse(value), Double.Parse(epsilon));
                            break;
                    }
                    break;
                case "ElementId":
                    switch (ruleType)
                    {
                        case "Equals":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateEqualsRule(paramId, new ElementId(Int32.Parse(value)));
                            break;
                        case "NotEquals":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotEqualsRule(paramId, new ElementId(Int32.Parse(value)));
                            break;
                        case "Greater":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateGreaterRule(paramId, new ElementId(Int32.Parse(value)));
                            break;
                        case "NotGreater":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateGreaterOrEqualRule(paramId, new ElementId(Int32.Parse(value)));
                            break;
                        case "GreaterOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateLessRule(paramId, new ElementId(Int32.Parse(value)));
                            break;
                        case "NotGreaterOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateLessOrEqualRule(paramId, new ElementId(Int32.Parse(value)));
                            break;
                    }
                    break;
                case "Integer":
                    switch (ruleType)
                    {
                        case "Equals":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateEqualsRule(paramId, Int32.Parse(value));
                            break;
                        case "NotEquals":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateNotEqualsRule(paramId, Int32.Parse(value));
                            break;
                        case "Greater":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateGreaterRule(paramId, Int32.Parse(value));
                            break;
                        case "NotGreater":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateGreaterOrEqualRule(paramId, Int32.Parse(value));
                            break;
                        case "GreaterOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateLessRule(paramId, Int32.Parse(value));
                            break;
                        case "NotGreaterOrEqual":
                            parameterFilterElement = ParameterFilterRuleFactory.CreateLessOrEqualRule(paramId, Int32.Parse(value));
                            break;
                    }
                    break;
            }

            return parameterFilterElement;
        }

        private ElementFilter CreateNewFilter(ElementId paramId, string type, string value)
        {
            var parameterFilterElement = CreateFilterRule(paramId, type, value);
            ElementFilter newFilter = new ElementParameterFilter(parameterFilterElement);
            return newFilter;
        }
    }
}
