﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.BLModels.SPF_logic;

namespace TGA_QM_Tools.BLModels
{
    public class AnnotationHostFamiliesHandler
    {
        private readonly Document document;
        private Dictionary<Guid, Guid> fakeGuidsDictionary = new Dictionary<Guid, Guid>();
        private Dictionary<Guid, string> fakeGuidsNameDictionary = new Dictionary<Guid, string>();
        private Dictionary<Guid, bool> parametersToDelete = new Dictionary<Guid, bool>();

        public AnnotationHostFamiliesHandler(Document doc)
        {
            document = doc;
        }

        public bool ReplaceSharedParametersWithFake(List<Guid> guidsToCheckInAnnotations, bool byNameOnly, out List<List<string>> tempTable, out bool nestedChanged)
        {
            nestedChanged = false;
            tempTable = new List<List<string>>();

            var famSPElements = new List<SharedParameterElement>();
            using (var col = new FilteredElementCollector(document))
            {
                famSPElements = col.OfClass(typeof(SharedParameterElement)).Cast<SharedParameterElement>().ToList();
            }

            if (famSPElements.Any(x => guidsToCheckInAnnotations.Any(z => z == x.GuidValue)))
            {
                nestedChanged = true;
                var famParameters = document.FamilyManager.Parameters.Cast<FamilyParameter>();

                foreach (var spElement in famSPElements.Where(x => guidsToCheckInAnnotations.Any(z => z == x.GuidValue)))
                {
                    var row = new List<string>();
                    var oldGuid = spElement.GuidValue;
                    var parameterName = spElement.Name;
                    var newParameter = document.ReplaceSharedParameterWithFake(spElement, famParameters.ToList(), out string oldName, out bool toDelete);
                    parametersToDelete.Add(newParameter.GUID, toDelete);
                    row.Add(document.PathName);
                    if (!byNameOnly)
                    {
                        row.Add(oldName);
                        row.Add("");
                        row.Add(oldGuid.ToString());
                    }
                    else
                    {
                        row.Add(oldGuid.ToString());
                        row.Add(parameterName);
                    }

                    if (newParameter != null)
                    {
                        fakeGuidsDictionary.Add(newParameter.GUID, oldGuid);
                        fakeGuidsNameDictionary.Add(newParameter.GUID, parameterName);
                    }
                    else
                    {
                        return false;
                    }

                    tempTable.Add(row);
                }
            }

            return true;
        }

        public bool ReplaceDummyParametersWithRenamedParameterWithTrans(List<List<string>> tempTable)
        {          
            try
            {
                var spHandler = new SharedParametersHandler(document);
                var spfReader = new ReadingSPF(document.Application);

                var famParameters = document.FamilyManager.Parameters.Cast<FamilyParameter>();

                foreach (var parameter in famParameters)
                {
                    if (parameter.IsShared && fakeGuidsDictionary.ContainsKey(parameter.GUID))
                    {
                        using (var tr = new Transaction(document, "fake parameter"))
                        {
                            tr.Start();

                            var extDef = spfReader.Definitions.FirstOrDefault(x => x.GUID == fakeGuidsDictionary[parameter.GUID]);
                            var toDelete = parametersToDelete[parameter.GUID];
                            var newParameter = spHandler.ReplaceWithSharedParameterSameType(parameter, extDef);
                            //if (document.OwnerFamily.FamilyCategory.CategoryType == CategoryType.Annotation &&
                            //    document.OwnerFamily.FamilyCategory.Id.IntegerValue != (int)BuiltInCategory.OST_GenericAnnotation
                            //    && toDelete)
                            //    document.FamilyManager.RemoveParameter(newParameter);

                            tempTable.FirstOrDefault(x => x[3] == extDef.GUID.ToString())[2] = extDef.Name;

                            tr.Commit();
                        }
                    }
                }
            }
            catch
            {
                return false;
            }

            return true;
        }

        public bool ReplaceDummyParametersWithRenamedParameterByNameWithTrans(List<List<string>> tempTable)
        {
            try
            {
                var spHandler = new SharedParametersHandler(document);
                var spfReader = new ReadingSPF(document.Application);

                var famParameters = document.FamilyManager.Parameters.Cast<FamilyParameter>();

                foreach (var parameter in famParameters)
                {
                    if (parameter.IsShared && fakeGuidsNameDictionary.ContainsKey(parameter.GUID))
                    {
                        using (var tr = new Transaction(document, "fake parameter"))
                        {
                            tr.Start();

                            var toDelete = parametersToDelete[parameter.GUID];
                            var extDef = spfReader.Definitions.FirstOrDefault(x => x.Name == fakeGuidsNameDictionary[parameter.GUID]);
                            var newParameter = spHandler.ReplaceWithSharedParameterSameType(parameter, extDef);
                            if (document.OwnerFamily.FamilyCategory.CategoryType == CategoryType.Annotation &&
                                document.OwnerFamily.FamilyCategory.Id.IntegerValue != (int)BuiltInCategory.OST_GenericAnnotation
                                && toDelete)
                                document.FamilyManager.RemoveParameter(newParameter);

                            tempTable.FirstOrDefault(x => x[2] == extDef.Name).Add(extDef.GUID.ToString());

                            tr.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
    }
}
