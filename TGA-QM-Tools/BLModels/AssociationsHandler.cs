﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.SPF_logic;

namespace TGA_QM_Tools.BLModels
{
    public class AssociationsHandler
    {
        private readonly Document document;
        private FamilyManager famManager;

        public AssociationsHandler(Document doc)
        {
            document = doc;
        }

        public bool ReassociateRenamedSharedParameter(ParameterAssociation association)
        {
            famManager = document.FamilyManager;
            if (association != null)
            {
                try
                {
                    var parameter = famManager.Parameters.Cast<FamilyParameter>().FirstOrDefault(x => x.IsShared && x.GUID == association.ParameterGuid
                                                                    || x.Id == association.ParameterId);
                    if (parameter != null)
                    {
                        var associatedParameters = parameter.AssociatedParameters.Cast<Parameter>();
                        foreach (var param in association.AssociatedParameters)
                        {
                            if (!associatedParameters.Any(x => param.ParameterName == x.Definition.Name
                                                                     && param.AssociatedElementId == x.Element.Id))
                            {
                                var element = document.GetElement(param.AssociatedElementId);
                                var elementParameter = element.Parameters.Cast<Parameter>().FirstOrDefault(x => x.Definition.Name == param.ParameterName);

                                if (elementParameter != null)
                                {
                                    famManager.AssociateElementParameterToFamilyParameter(elementParameter, parameter);
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool ReassociateRenamedNestedSharedParameter(ParameterAssociation association)
        {
            famManager = document.FamilyManager;
            if (association != null)
            {
                try
                {
                    var parameter = famManager.Parameters.Cast<FamilyParameter>().FirstOrDefault(x => x.IsShared && x.GUID == association.ParameterGuid 
                                                                            || x.Id == association.ParameterId);
                    if (parameter != null)
                    {
                        var associatedParameters = parameter.AssociatedParameters.Cast<Parameter>();
                        foreach (var param in association.AssociatedParameters)
                        {
                            if (!associatedParameters.Any(x => param.ParameterName == x.Definition.Name && param.AssociatedElementId == x.Element.Id))
                            {
                                var element = document.GetElement(param.AssociatedElementId);
                                var elementParameter = element.Parameters.Cast<Parameter>().FirstOrDefault(x => x.IsShared && x.GUID == param.ParameterGuid
                                                                                                       || !x.IsShared && x.Definition.Name == param.ParameterName);

                                if (elementParameter != null)
                                {
                                    famManager.AssociateElementParameterToFamilyParameter(elementParameter, parameter);
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public bool ReassociateRenamedFamilyParameter(ParameterAssociation association)
        {
            famManager = document.FamilyManager;
            if (association != null)
            {
                try
                {
                    var parameter = famManager.Parameters.Cast<FamilyParameter>().FirstOrDefault(x => x.Definition.Name == association.ParameterName
                                                            || x.Id == association.ParameterId);
                    if (parameter != null)
                    {
                        var associatedParameters = parameter.AssociatedParameters.Cast<Parameter>();
                        foreach (var param in association.AssociatedParameters)
                        {
                            if (!associatedParameters.Any(x => param.ParameterName == x.Definition.Name && param.AssociatedElementId == x.Element.Id))
                            {
                                var element = document.GetElement(param.AssociatedElementId);
                                var elementParameter = element.Parameters.Cast<Parameter>().FirstOrDefault(x => x.Definition.Name == param.ParameterName);

                                if (elementParameter != null)
                                {
                                    famManager.AssociateElementParameterToFamilyParameter(elementParameter, parameter);
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        public bool ReassociateRenamedSharedParameterByName(ParameterAssociation association)
        {
            famManager = document.FamilyManager;
            if (association != null)
            {
                try
                {
                    var parameter = famManager.Parameters.Cast<FamilyParameter>().FirstOrDefault(x => x.IsShared && x.Definition.Name == association.ParameterName
                                                        || x.Id == association.ParameterId);
                    if (parameter != null)
                    {
                        var associatedParameters = parameter.AssociatedParameters.Cast<Parameter>();
                        foreach (var param in association.AssociatedParameters)
                        {
                            if (!associatedParameters.Any(x => param.ParameterName == x.Definition.Name && param.AssociatedElementId == x.Element.Id))
                            {
                                var element = document.GetElement(param.AssociatedElementId);
                                var elementParameter = element.Parameters.Cast<Parameter>().FirstOrDefault(x => x.Definition.Name == param.ParameterName);

                                if (elementParameter != null)
                                {
                                    famManager.AssociateElementParameterToFamilyParameter(elementParameter, parameter);
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public void RemapReplacedSharedWithFamilyParameter(FamilyParameter parameter, ParameterAssociation association)
        {
            if (association != null)
            {
                association.ParameterId = parameter.Id;
                association.ParameterName = parameter.Definition.Name;
            }
        }

        public void RemapReplacedSharedWithDifferentGuids(FamilyParameter parameter, ParameterAssociation association)
        {
            if (association != null)
            {
                association.ParameterId = parameter.Id;
                association.ParameterGuid = parameter.GUID;
            }
        }

        public void RemapRenamedFamilyParameter(FamilyParameter parameter, ParameterAssociation association, string newName)
        {
            if (association != null)
            {
                association.ParameterId = parameter.Id;
                association.ParameterName = newName;
            }
        }

        public bool ReassociateReplacedSharedWithFamilyParameter(ParameterAssociation association)
        {
            famManager = document.FamilyManager;
            if (association != null)
            {
                try
                {
                    var parameter = document.FamilyManager.Parameters.Cast<FamilyParameter>().FirstOrDefault(x => x.Id == association.ParameterId);
                    if (parameter != null)
                    {
                        var associatedParameters = parameter.AssociatedParameters.Cast<Parameter>();
                        foreach (var param in association.AssociatedParameters)
                        {
                            if (!associatedParameters.Any(x => param.ParameterName == x.Definition.Name && param.AssociatedElementId == x.Element.Id))
                            {
                                var element = document.GetElement(param.AssociatedElementId);
                                var elementParameter = element.Parameters.Cast<Parameter>().FirstOrDefault(x => x.IsShared && x.GUID == param.ParameterGuid);
                                using (var tr = new Transaction(document, "Reassociation"))
                                {
                                    tr.Start();
                                    if (elementParameter != null)
                                    {
                                        famManager.AssociateElementParameterToFamilyParameter(elementParameter, parameter);
                                        tr.Commit();
                                    }
                                    else
                                    {
                                        tr.RollBack();
                                        return false;
                                    }
                                }
                            }
                        }

                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
    }
}
