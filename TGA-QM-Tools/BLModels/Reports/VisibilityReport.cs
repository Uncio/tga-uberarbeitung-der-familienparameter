﻿using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.BLModels.Reports
{
    public class VisibilityReport
    {
        public void Report(Application app, List<string> familyFiles, string prefix)
        {
            var table = new List<List<string>>();
            table.Add(new List<string> { "Familienname", "Familienkategorie", "SolidId", "Unterkategorie", "Sichtbarkeit" });

            var title = "Process files";
            var maxValue = familyFiles.Count();
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var familyFile in familyFiles)
                {
                    var famDoc = app.OpenDocumentFile(familyFile);
                    try
                    {
                        using (var col = new FilteredElementCollector(famDoc).OfClass(typeof(GenericForm)))
                        {
                            var solids = col.ToElements();
                            foreach (var solid in solids)
                            {
                                var visibility = solid.get_Parameter(BuiltInParameter.GEOM_VISIBILITY_PARAM).AsInteger();
                                var row = new List<string>();
                                row.Add(famDoc.Title);
                                row.Add(famDoc.OwnerFamily.FamilyCategory.Name);
                                row.Add(solid.Id.IntegerValue.ToString());
                                if (solid.Category != null)
                                {
                                    row.Add(solid.Category.Name);
                                }
                                else
                                {
                                    row.Add("");
                                }
                                row.Add(visibility.ToString());
                                table.Add(row);
                            }
                        }

                        famDoc.Close(false);
                    }
                    catch
                    {
                        if (famDoc.IsValidObject)
                        {
                            famDoc.Close();
                        }
                    }
                    pf.Increment();
                }
            }

            table.WriteCsvTo(prefix + "Visibility.csv");
        }
    }
}
