﻿using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.BLModels.Reports
{
    class FamiliesParametersReport
    {
        private string familyOver2020 = String.Empty;
        private string failedFamilies = String.Empty;

        public void Report(Application app, List<string> familyFiles, string prefix)
        {
            var table = new List<List<string>>();
            table.Add(new List<string>() {
                "FamilienName",
                "TypName",
                "FamilienKategorie",
                "FamilienKategorie(OST)",
                "ParameterName",
                "ParameterWert",

                "ParameterTyp",
                "ParameterUnitType",
                "GUID",
                "PlandataGruppe",
                "ParameterGruppe",

                "ParameterGruppe(OST)",
                "Formula",
                "ZugehörigerParameterName",
                "ZugehörigerElementId",
                "ZugehörigerElementType",

                "ZugehörigerAbmessungen",
                "IfcExportAs",
                "DIN 276",
                "OEN 1801",
                "UniClass",

                "OMNICLASS",
                "inUse?" });

            var title = "Process files";
            var maxValue = familyFiles.Count();
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    var guidList = new List<Guid>();
                    Document famDoc = null;
                    try
                    {
                        famDoc = app.OpenDocumentFile(file);
                    }
                    catch
                    {
                        familyOver2020 += file + Environment.NewLine;
                        continue;
                    }

                    try
                    {
                        var paramAssDic = new Dictionary<string, List<string>>();
                        var parameters = famDoc.FamilyManager.Parameters;

                        foreach (FamilyParameter par in parameters)
                        {
                            var assPars = par.AssociatedParameters;
                            if (assPars.Size != 0)
                                paramAssDic.Add(par.Definition.Name, new List<string>());
                            foreach (Parameter para in assPars)
                            {
                                paramAssDic[par.Definition.Name].Add(para.Definition.Name);
                            }

                            var types = famDoc.FamilyManager.Types;

                            var plandataParameter = famDoc.FamilyManager.get_Parameter(new Guid("8a7b2751-6676-4816-b7b0-cd07abbb7813"));
                            var ifcParameter = famDoc.FamilyManager.get_Parameter(new Guid("f53d1285-ae3d-4992-a3f1-2e7978be529a"));
                            var DIN276Parameter = famDoc.FamilyManager.get_Parameter(new Guid("0c0dc952-fee9-445a-8bb1-2a1812250cc3"));
                            var OEN1801Parameter = famDoc.FamilyManager.get_Parameter(new Guid("fe9e83eb-e164-445c-a19d-e7dcd11a52b1"));
                            var uniClassParameter = famDoc.FamilyManager.get_Parameter(new Guid("1cad782c-f96c-4274-aef9-95f8b22eabbc"));
                            var omniClassParameter = famDoc.FamilyManager.get_Parameter(new Guid("e8599841-114b-4721-b44e-3561bed5d4ea"));

                            string plandataGruppe = String.Empty;
                            if (plandataParameter != null)
                            {
                                plandataGruppe = famDoc.FamilyManager.CurrentType.AsString(plandataParameter);
                            }

                            foreach(FamilyType type in types)
                            {
                                if (!assPars.IsEmpty)
                                {
                                    foreach (Parameter assPar in assPars)
                                    {
                                        var row = new List<string>();
                                        row.Add(famDoc.Title);
                                        row.Add(type.Name);
                                        row.Add(LabelUtils.GetLabelFor((BuiltInCategory)famDoc.OwnerFamily.FamilyCategory.Id.IntegerValue));
                                        row.Add(((BuiltInCategory)(famDoc.OwnerFamily.FamilyCategory.Id.IntegerValue)).ToString());
                                        row.Add(par.Definition.Name);

                                        var value = String.Empty;
                                        if (par.StorageType == StorageType.String)
                                        {
                                            value = type.AsString(par);
                                        }
                                        else
                                        {
                                            value = type.AsValueString(par);
                                        }
                                        row.Add(value != null ? value : "");

                                        if (par.IsShared)
                                        {
                                            guidList.Add(par.GUID);
                                            row.Add("SharedParameter");
                                        }
                                        else if ((par.Definition as InternalDefinition).BuiltInParameter != BuiltInParameter.INVALID)
                                            row.Add("BuiltInParameter");
                                        else row.Add("FamilyParameter");
                                        row.Add(par.Definition.ParameterType.ToString());
                                        if (par.IsShared)
                                            row.Add(par.GUID.ToString());
                                        else
                                            row.Add("");
                                        row.Add(plandataGruppe);

                                        row.Add(LabelUtils.GetLabelFor(par.Definition.ParameterGroup));
                                        row.Add(par.Definition.ParameterGroup.ToString());
                                        row.Add(par.Formula);
                                        row.Add(assPar.Definition.Name);
                                        row.Add(assPar.Element.Id.IntegerValue.ToString());

                                        row.Add(assPar.Element.GetType().Name);
                                        row.Add(GetAssociatedDimensions(famDoc, par));
                                        row.Add(ifcParameter != null ? type.AsString(ifcParameter) : "");
                                        row.Add(DIN276Parameter != null ? type.AsString(DIN276Parameter) : "");
                                        row.Add(OEN1801Parameter != null ? type.AsString(OEN1801Parameter) : "");
                                        row.Add(uniClassParameter != null ? type.AsString(uniClassParameter) : "");
                                        row.Add(omniClassParameter != null ? type.AsString(omniClassParameter) : "");

                                        row.Add(par.CheckParameter(famDoc, null));

                                        table.Add(row);
                                    }
                                }
                                else
                                {
                                    var row = new List<string>();
                                    row.Add(famDoc.Title);
                                    row.Add(type.Name);
                                    row.Add(LabelUtils.GetLabelFor((BuiltInCategory)famDoc.OwnerFamily.FamilyCategory.Id.IntegerValue));
                                    row.Add(((BuiltInCategory)(famDoc.OwnerFamily.FamilyCategory.Id.IntegerValue)).ToString());
                                    row.Add(par.Definition.Name);

                                    var value = String.Empty;
                                    if (par.StorageType == StorageType.String)
                                    {
                                        value = type.AsString(par);
                                    }
                                    else
                                    {
                                        value = type.AsValueString(par);
                                    }

                                    row.Add(value != null ? value : "");
                                    if (par.IsShared)
                                    {
                                        guidList.Add(par.GUID);
                                        row.Add("SharedParameter");
                                    }
                                    else if ((par.Definition as InternalDefinition).BuiltInParameter != BuiltInParameter.INVALID)
                                        row.Add("BuiltInParameter");
                                    else row.Add("FamilyParameter");
                                    row.Add(par.Definition.ParameterType.ToString());
                                    if (par.IsShared)
                                        row.Add(par.GUID.ToString());
                                    else
                                        row.Add("");
                                    row.Add(plandataGruppe);
                                    row.Add(LabelUtils.GetLabelFor(par.Definition.ParameterGroup));
                                    row.Add(par.Definition.ParameterGroup.ToString());
                                    row.Add(par.Formula);
                                    row.Add("");
                                    row.Add("");
                                    row.Add("");
                                    row.Add(GetAssociatedDimensions(famDoc, par));
                                    row.Add(ifcParameter != null ? type.AsString(ifcParameter) : "");
                                    row.Add(DIN276Parameter != null ? type.AsString(DIN276Parameter) : "");
                                    row.Add(OEN1801Parameter != null ? type.AsString(OEN1801Parameter) : "");
                                    row.Add(uniClassParameter != null ? type.AsString(uniClassParameter) : "");
                                    row.Add(omniClassParameter != null ? type.AsString(omniClassParameter) : "");
                                    row.Add(par.CheckParameter(famDoc, null));

                                    table.Add(row);
                                }                              
                            }                            
                        }

                        var nestedFamilies = new FilteredElementCollector(famDoc).OfClass(typeof(Family)).Where(x => (x as Family).IsEditable).Cast<Family>().ToList();

                        table.AddRange(ReportNestedFamilyParameters(famDoc, nestedFamilies, famDoc.Title, paramAssDic));


                    }
                    finally
                    {
                        if (famDoc != null && famDoc.IsValidObject)
                        {
                            famDoc.Close(false);
                        }
                        else
                        {
                            failedFamilies += file + Environment.NewLine;
                        }
                    }

                    pf.Increment();
                }
            }

            failedFamilies.WriteToTxt(prefix + "FailedFamilies.txt");
            familyOver2020.WriteToTxt(prefix + "FamiliesOver2020.txt");
            table.WriteCsvTo(prefix + "FamilyParametersReport.csv");
        }

        private List<List<string>> ReportNestedFamilyParameters(Document famDoc,
                                                        List<Family> nestedFamilies,
                                                        string parentName,
                                                        Dictionary<string, List<string>> paramAssDic)
        {
            var result = new List<List<string>>();

            if (nestedFamilies.Count == 0)
            {
                return result;
            }

            foreach (Family itemFam in nestedFamilies)
            {
                var table = new List<List<string>>();
                var nestedFamGuidList = new List<Guid>();
                var nestedFamDoc = famDoc.EditFamily(itemFam);
                try
                {
                    var nestedParamAssDic = new Dictionary<string, List<string>>();
                    var nestedParams = nestedFamDoc.FamilyManager.Parameters;

                    foreach (FamilyParameter par in nestedParams)
                    {
                        var assPars = par.AssociatedParameters;
                        if (assPars.Size != 0)
                            nestedParamAssDic.Add(par.Definition.Name, new List<string>());
                        foreach (Parameter para in assPars)
                        {
                            nestedParamAssDic[par.Definition.Name].Add(para.Definition.Name);
                        }

                        var types = nestedFamDoc.FamilyManager.Types;

                        var plandataParameter = nestedFamDoc.FamilyManager.get_Parameter(new Guid("8a7b2751-6676-4816-b7b0-cd07abbb7813"));
                        var ifcParameter = nestedFamDoc.FamilyManager.get_Parameter(new Guid("f53d1285-ae3d-4992-a3f1-2e7978be529a"));
                        var DIN276Parameter = nestedFamDoc.FamilyManager.get_Parameter(new Guid("0c0dc952-fee9-445a-8bb1-2a1812250cc3"));
                        var OEN1801Parameter = nestedFamDoc.FamilyManager.get_Parameter(new Guid("fe9e83eb-e164-445c-a19d-e7dcd11a52b1"));
                        var uniClassParameter = nestedFamDoc.FamilyManager.get_Parameter(new Guid("1cad782c-f96c-4274-aef9-95f8b22eabbc"));
                        var omniClassParameter = nestedFamDoc.FamilyManager.get_Parameter(new Guid("e8599841-114b-4721-b44e-3561bed5d4ea"));

                        string plandataGruppe = String.Empty;
                        if (plandataParameter != null)
                        {
                            plandataGruppe = nestedFamDoc.FamilyManager.CurrentType.AsValueString(plandataParameter);
                        }

                        foreach (FamilyType type in types)
                        {
                            if (!assPars.IsEmpty)
                            {
                                foreach (Parameter assPar in assPars)
                                {
                                    var row = new List<string>();
                                    row.Add(parentName + "/" + nestedFamDoc.Title);
                                    row.Add(type.Name);
                                    row.Add(LabelUtils.GetLabelFor((BuiltInCategory)nestedFamDoc.OwnerFamily.FamilyCategory.Id.IntegerValue));
                                    row.Add(((BuiltInCategory)(nestedFamDoc.OwnerFamily.FamilyCategory.Id.IntegerValue)).ToString());
                                    row.Add(par.Definition.Name);

                                    var value = String.Empty;
                                    if (par.StorageType == StorageType.String)
                                    {
                                        value = type.AsString(par);
                                    }
                                    else
                                    {
                                        value = type.AsValueString(par);
                                    }
                                    row.Add(value != null ? value : "");

                                    if (par.IsShared)
                                    {
                                        row.Add("SharedParameter");
                                    }
                                    else if ((par.Definition as InternalDefinition).BuiltInParameter != BuiltInParameter.INVALID)
                                        row.Add("BuiltInParameter");
                                    else row.Add("FamilyParameter");
                                    row.Add(par.Definition.ParameterType.ToString());
                                    if (par.IsShared)
                                        row.Add(par.GUID.ToString());
                                    else
                                        row.Add("");
                                    row.Add(plandataGruppe);

                                    row.Add(LabelUtils.GetLabelFor(par.Definition.ParameterGroup));
                                    row.Add(par.Definition.ParameterGroup.ToString());
                                    row.Add(par.Formula);
                                    row.Add(assPar.Definition.Name);
                                    row.Add(assPar.Element.Id.IntegerValue.ToString());

                                    row.Add(assPar.Element.GetType().Name);
                                    row.Add(GetAssociatedDimensions(nestedFamDoc, par));
                                    row.Add(ifcParameter != null ? type.AsString(ifcParameter) : "");
                                    row.Add(DIN276Parameter != null ? type.AsString(DIN276Parameter) : "");
                                    row.Add(OEN1801Parameter != null ? type.AsString(OEN1801Parameter) : "");
                                    row.Add(uniClassParameter != null ? type.AsString(uniClassParameter) : "");
                                    row.Add(omniClassParameter != null ? type.AsString(omniClassParameter) : "");

                                    row.Add(par.CheckParameter(nestedFamDoc, paramAssDic));

                                    table.Add(row);
                                }
                            }
                            else
                            {
                                var row = new List<string>();
                                row.Add(parentName + "/" + nestedFamDoc.Title);
                                row.Add(type.Name);
                                row.Add(LabelUtils.GetLabelFor((BuiltInCategory)nestedFamDoc.OwnerFamily.FamilyCategory.Id.IntegerValue));
                                row.Add(((BuiltInCategory)(nestedFamDoc.OwnerFamily.FamilyCategory.Id.IntegerValue)).ToString());
                                row.Add(par.Definition.Name);

                                var value = String.Empty;
                                if (par.StorageType == StorageType.String)
                                {
                                    value = type.AsString(par);
                                }
                                else
                                {
                                    value = type.AsValueString(par);
                                }
                                row.Add(value != null ? value : "");

                                if (par.IsShared)
                                {
                                    row.Add("SharedParameter");
                                }
                                else if ((par.Definition as InternalDefinition).BuiltInParameter != BuiltInParameter.INVALID)
                                    row.Add("BuiltInParameter");
                                else row.Add("FamilyParameter");
                                row.Add(par.Definition.ParameterType.ToString());
                                if (par.IsShared)
                                    row.Add(par.GUID.ToString());
                                else
                                    row.Add("");
                                row.Add(plandataGruppe);
                                row.Add(LabelUtils.GetLabelFor(par.Definition.ParameterGroup));
                                row.Add(par.Definition.ParameterGroup.ToString());
                                row.Add(par.Formula);
                                row.Add("");
                                row.Add("");
                                row.Add("");
                                row.Add(GetAssociatedDimensions(nestedFamDoc, par));
                                row.Add(ifcParameter != null ? type.AsString(ifcParameter) : "");
                                row.Add(DIN276Parameter != null ? type.AsString(DIN276Parameter) : "");
                                row.Add(OEN1801Parameter != null ? type.AsString(OEN1801Parameter) : "");
                                row.Add(uniClassParameter != null ? type.AsString(uniClassParameter) : "");
                                row.Add(omniClassParameter != null ? type.AsString(omniClassParameter) : "");
                                row.Add(par.CheckParameter(nestedFamDoc, paramAssDic));

                                table.Add(row);
                            }
                        }
                    }

                    result.AddRange(table);

                    var nextNestedFamilies = new FilteredElementCollector(nestedFamDoc).OfClass(typeof(Family)).Where(x => (x as Family).IsEditable).Cast<Family>().ToList();

                    var nestedReport = ReportNestedFamilyParameters(nestedFamDoc, nextNestedFamilies, parentName + "/" + nestedFamDoc.Title, nestedParamAssDic);
                    if (nestedReport.Count != 0)
                    {
                        result.AddRange(nestedReport);
                    }
                }
                finally
                {
                    if (nestedFamDoc != null && nestedFamDoc.IsValidObject)
                    {
                        nestedFamDoc.Close(false);
                    }
                    else
                    {
                        failedFamilies += "nested_" + itemFam.Name + Environment.NewLine;
                    }
                }

            }

            return result;
        }

        public string GetAssociatedDimensions(Document doc, FamilyParameter parameter)
        {
            var result = String.Empty;

            var dimensions = new List<Element>();
            using (var col = new FilteredElementCollector(doc).OfClass(typeof(Dimension)))
            {
                dimensions = col.Where(x =>
                {
                    var res = false;
                    try
                    {
                        res = (x as Dimension).FamilyLabel != null && (x as Dimension).FamilyLabel.Id == parameter.Id;
                    }
                    catch { }
                    return res;
                }).ToList();
            }

            dimensions.ForEach(x => result += x.Id.IntegerValue.ToString() + ";");

            return result;
        }
    }
}
