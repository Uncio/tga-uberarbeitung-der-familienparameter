﻿using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.BLModels.Helpers;
using TGA_QM_Tools.Models;

namespace TGA_QM_Tools.BLModels.Reports
{
    internal class TemplateParametersReport
    {
        private readonly UIApplication uiApp;
        private List<SharedParameterData> parametersData;

        public TemplateParametersReport(UIApplication uIApplication)
        {
            uiApp = uIApplication;
        }

        public void CollectData(string tgaSpf, string bauSpf)
        {
            var initialSpf = uiApp.Application.SharedParametersFilename;

            var doc = uiApp.ActiveUIDocument.Document;
            parametersData = new List<SharedParameterData>();

            using(var col = new FilteredElementCollector(doc))
            {
                var allSharedParameters = col.OfClass(typeof(SharedParameterElement)).Cast<SharedParameterElement>().ToList();
                allSharedParameters.ForEach(param => parametersData.Add(new SharedParameterData { SharedParameter = param }));
            }

            //parameters from project
            var map = doc.ParameterBindings;
            var iterator = map.ForwardIterator();
            while (iterator.MoveNext())
            {
                var paramId = (iterator.Key as InternalDefinition).Id;
                var param = doc.GetElement(paramId);

                if(param is SharedParameterElement sharedParam)
                {
                    parametersData.FirstOrDefault(x => x.SharedParameter.GuidValue == sharedParam.GuidValue).IsProject = true;

                    if (iterator.Current is InstanceBinding)
                        parametersData.FirstOrDefault(x => x.SharedParameter.GuidValue == sharedParam.GuidValue).IsProjectInstance = true;
                    var categories = (iterator.Current as ElementBinding).Categories.Cast<Category>().ToList();
                    categories.ForEach(x =>
                        parametersData.FirstOrDefault(z => z.SharedParameter.GuidValue == sharedParam.GuidValue).Categories.Add((BuiltInCategory)x.Id.IntegerValue));
                }
            }

            //prameters from families
            var families = new List<Family>();

            using (var col = new FilteredElementCollector(doc))
            {
                families = col.OfClass(typeof(Family)).Cast<Family>()
                    .Where(f => f.IsEditable)
                    .ToList();
            }

            foreach (var family in families)
            {
                var famDoc = doc.EditFamily(family);

                using (var col = new FilteredElementCollector(famDoc))
                {
                    var allSharedParameters = col.OfClass(typeof(SharedParameterElement)).Cast<SharedParameterElement>().ToList();
                    allSharedParameters.ForEach(param => {
                        if (parametersData.Any(x => x.SharedParameter.GuidValue == param.GuidValue))
                        {
                            parametersData.FirstOrDefault(x => x.SharedParameter.GuidValue == param.GuidValue).IsInFamily = true;

                            var famParam = famDoc.FamilyManager.Parameters.Cast<FamilyParameter>()
                                .FirstOrDefault(x => x.IsShared && x.GUID == param.GuidValue);
                            if (famParam != null)
                                parametersData.FirstOrDefault(x => x.SharedParameter.GuidValue == param.GuidValue).IsFamilyInstance = famParam.IsInstance;
                        }
                    });
                }

                if (famDoc.IsValidObject)
                {
                    famDoc.Close(false);
                }
            }

            //parameters from filters
            var filterUtil = new FiltersLibraryUtil(doc);
            var filters = filterUtil.CreateFiltersLibrary();
            var viewsDic = filterUtil.CreateViewsLibrary(filters);

            foreach (var view in viewsDic)
            {
                if (view.Value.Count() != 0)
                {
                    foreach (var filter in view.Value)
                    {
                        if (filter.Parameters.Count() != 0)
                        {
                            foreach (var parameter in filter.Parameters)
                            {
                                if(parametersData.Any(x => x.SharedParameter.Name == parameter))
                                {
                                    parametersData.FirstOrDefault(x => x.SharedParameter.Name == parameter).IsInFilters = true;
                                    parametersData.FirstOrDefault(x => x.SharedParameter.Name == parameter).FilterNames += filter.FilterName + ";";
                                }
                            }
                        }
                    }
                }
            }

            //parameters from Schedules
            var allSchedules = new List<ViewSchedule>();
            using (var col = new FilteredElementCollector(doc))
            {
                allSchedules = col.OfClass(typeof(ViewSchedule)).Cast<ViewSchedule>().Where(x => !x.IsTemplate).ToList();
            }

            foreach(var schedule in allSchedules)
            {
                var tableData = schedule.Definition;
                var fieldsCount = tableData.GetFieldCount();
                for(int i = 0; i < fieldsCount; i++)
                {
                    var field = tableData.GetField(i);
                    var param = doc.GetElement(field.ParameterId);

                    if (param is SharedParameterElement sharedParam && parametersData.Any(x => x.SharedParameter.GuidValue == sharedParam.GuidValue))
                    {
                        parametersData.FirstOrDefault(x => x.SharedParameter.GuidValue == sharedParam.GuidValue).IsInSchedules = true;
                        parametersData.FirstOrDefault(x => x.SharedParameter.GuidValue == sharedParam.GuidValue).ScheduleNames += schedule.Name + ";";
                    }
                }


            }

            //comparing with TGA SPF
            uiApp.Application.SharedParametersFilename = tgaSpf;
            var spf = uiApp.Application.OpenSharedParameterFile();
            foreach(var group in spf.Groups)
            {
                foreach(var definition in group.Definitions)
                {
                    if (parametersData.Any(x => x.SharedParameter.Name == definition.Name))
                    {
                        parametersData.FirstOrDefault(x => x.SharedParameter.Name == definition.Name).IsTGAspfName = true;
                    }

                    if (parametersData.Any(x => x.SharedParameter.GuidValue == (definition as ExternalDefinition).GUID))
                    {
                        parametersData.FirstOrDefault(x => x.SharedParameter.GuidValue == (definition as ExternalDefinition).GUID).IsTGAspfGuid = true;
                    }
                }
            }

            //comparing with BAU SPF
            uiApp.Application.SharedParametersFilename = bauSpf;
            spf = uiApp.Application.OpenSharedParameterFile();
            foreach (var group in spf.Groups)
            {
                foreach (var definition in group.Definitions)
                {
                    if (parametersData.Any(x => x.SharedParameter.Name == definition.Name))
                    {
                        parametersData.FirstOrDefault(x => x.SharedParameter.Name == definition.Name).IsBAUspfName = true;
                    }

                    if (parametersData.Any(x => x.SharedParameter.GuidValue == (definition as ExternalDefinition).GUID))
                    {
                        parametersData.FirstOrDefault(x => x.SharedParameter.GuidValue == (definition as ExternalDefinition).GUID).IsBAUspfGuid = true;
                    }
                }
            }

            if (!String.IsNullOrEmpty(initialSpf))
            {
                uiApp.Application.SharedParametersFilename = initialSpf;
            }
        }

        public void Report(string folder)
        {
            var table = new List<List<string>>();
            table.Add(new List<string>()
            {
                "ParameterName",
                "ParameterTyp",
                "GUID",
                "In Familie",
                "Instace oder Typ in Familie",
                "In Projekt",
                "Instace oder Typ in Projekt",
                "BindCategory",
                "In Filter",
                "Filter Name",
                "In Bauteilliste",
                "Bautelliste Name",
                "TGA Name",
                "GA GUID",
                "BAU Name",
                "BAU GUID"
            });
            foreach(var data in parametersData)
            {
                var row = new List<string>();
                row.Add(data.SharedParameter.Name);
                row.Add(data.SharedParameter.GetDefinition().ParameterType.ToString());
                row.Add(data.SharedParameter.GuidValue.ToString());
                row.Add(data.IsInFamily.ToString());
                row.Add(data.IsInFamily ? data.IsFamilyInstance.ToString() : "");
                row.Add(data.IsProject.ToString());
                row.Add(data.IsProject ? data.IsProjectInstance.ToString() : "");
                if (data.IsProject)
                {
                    string cats = "";
                    data.Categories.ForEach(x => cats += x + ";");
                    row.Add(cats);
                }
                else
                {
                    row.Add("");
                }
                row.Add(data.IsInFilters.ToString());
                row.Add(data.FilterNames);
                row.Add(data.IsInSchedules.ToString());
                row.Add(data.ScheduleNames);
                row.Add(data.IsTGAspfName.ToString());
                row.Add(data.IsTGAspfGuid.ToString());
                row.Add(data.IsBAUspfName.ToString());
                row.Add(data.IsBAUspfGuid.ToString());

                table.Add(row);
            }

            table.WriteCsvTo("TemplateParametersReport.csv", folder);
        }
    }
}
