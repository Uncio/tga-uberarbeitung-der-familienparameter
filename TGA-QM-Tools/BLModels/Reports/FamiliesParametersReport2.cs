﻿using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.Views;

namespace TGA_QM_Tools.BLModels.Reports
{
    class FamiliesParametersReport2
    {
        private string familyOver2020 = String.Empty;
        private string failedFamilies = String.Empty;
        public void Report(Application app, List<string> familyFiles, string prefix)
        {
            var table = new List<List<string>>();
            table.Add(new List<string>() {
                "FamilyName",
                "FamilyCategory",
                "Parameter",
                "ParameterGroup",
                "GUID",
                "ParameterType",
                "TypeOfParameter",
                "inUse?" });

            var title = "Process files";
            var maxValue = familyFiles.Count();
            string s = "{0} of " + maxValue.ToString() + " files processed...";
            using (ProgressBar pf = new ProgressBar(title, s, maxValue))
            {
                foreach (var file in familyFiles)
                {
                    var guidList = new List<Guid>();
                    Document famDoc = null;
                    try
                    {
                        famDoc = app.OpenDocumentFile(file);
                    }
                    catch
                    {
                        familyOver2020 += file + Environment.NewLine;
                        continue;
                    }

                    try
                    {
                        var paramAssDic = new Dictionary<string, List<string>>();
                        var parameters = famDoc.FamilyManager.Parameters;

                        foreach (FamilyParameter par in parameters)
                        {
                            var assPars = par.AssociatedParameters;
                            if (assPars.Size != 0)
                                paramAssDic.Add(par.Definition.Name, new List<string>());
                            foreach (Parameter para in assPars)
                            {
                                paramAssDic[par.Definition.Name].Add(para.Definition.Name);
                            }

                            var row = new List<string>();
                            row.Add(famDoc.Title);
                            row.Add(famDoc.OwnerFamily.FamilyCategory.Name);
                            row.Add(par.Definition.Name);
                            row.Add(par.Definition.ParameterGroup.ToString());
                            if (par.IsShared)
                                row.Add(par.GUID.ToString());
                            else
                                row.Add("");
                            row.Add(par.Definition.ParameterType.ToString());
                            if (par.IsShared)
                            {
                                guidList.Add(par.GUID);
                                row.Add("SharedParameter");
                            }
                            else if ((par.Definition as InternalDefinition).BuiltInParameter != BuiltInParameter.INVALID)
                                row.Add("BuiltInParameter");
                            else row.Add("FamilyParameter");

                            row.Add(par.CheckParameter(famDoc, null));
                            table.Add(row);
                        }

                        var additionalSPElements = new List<SharedParameterElement>();
                        using (var col = new FilteredElementCollector(famDoc).OfClass(typeof(SharedParameterElement)))
                        {
                            additionalSPElements.AddRange(col.Where(x => !guidList.Any(z => z == (x as SharedParameterElement).GuidValue)).Cast<SharedParameterElement>());
                        }

                        foreach (var para in additionalSPElements)
                        {
                            var row = new List<string>();
                            row.Add(famDoc.Title);
                            row.Add(famDoc.OwnerFamily.FamilyCategory.Name);
                            row.Add(para.Name);
                            row.Add(para.GetDefinition().ParameterGroup.ToString());
                            row.Add(para.GuidValue.ToString());
                            row.Add(para.GetDefinition().ParameterType.ToString());
                            row.Add("SharedParameterElement");
                            row.Add("");
                            table.Add(row);
                        }

                        var nestedFamilies = new FilteredElementCollector(famDoc).OfClass(typeof(Family)).Where(x => (x as Family).IsEditable).Cast<Family>().ToList();

                        table.AddRange(ReportNestedFamilyParameters(famDoc, nestedFamilies, famDoc.Title, paramAssDic));


                    }
                    finally
                    {
                        if (famDoc != null && famDoc.IsValidObject)
                        {
                            famDoc.Close(false);
                        }
                        else
                        {
                            failedFamilies += file + Environment.NewLine;
                        }
                    }

                    pf.Increment();
                }
            }

            failedFamilies.WriteToTxt(prefix + "FailedFamilies.txt");
            familyOver2020.WriteToTxt(prefix + "FamiliesOver2020.txt");
            table.WriteCsvTo(prefix + "FamilyParametersReport.csv");
        }

        private List<List<string>> ReportNestedFamilyParameters(Document famDoc,
                                                        List<Family> nestedFamilies,
                                                        string parentName,
                                                        Dictionary<string, List<string>> paramAssDic)
        {
            var result = new List<List<string>>();

            if (nestedFamilies.Count == 0)
            {
                return result;
            }

            foreach (Family itemFam in nestedFamilies)
            {
                var table = new List<List<string>>();
                var nestedFamGuidList = new List<Guid>();
                var nestedFamDoc = famDoc.EditFamily(itemFam);
                try
                {
                    var nestedParamAssDic = new Dictionary<string, List<string>>();
                    var nestedParams = nestedFamDoc.FamilyManager.Parameters;

                    foreach (FamilyParameter par in nestedParams)
                    {
                        var assPars = par.AssociatedParameters;
                        if (assPars.Size != 0)
                            nestedParamAssDic.Add(par.Definition.Name, new List<string>());
                        foreach (Parameter para in assPars)
                        {
                            nestedParamAssDic[par.Definition.Name].Add(para.Definition.Name);
                        }

                        var row = new List<string>();
                        row.Add(parentName + "/" + nestedFamDoc.Title);
                        row.Add(itemFam.FamilyCategory.Name);
                        row.Add(par.Definition.Name);
                        row.Add(par.Definition.ParameterGroup.ToString());
                        if (par.IsShared)
                        {
                            nestedFamGuidList.Add(par.GUID);
                            row.Add(par.GUID.ToString());
                        }
                        else
                            row.Add("");
                        row.Add(par.Definition.ParameterType.ToString());
                        if (par.IsShared)
                            row.Add("SharedParameter");
                        else if ((par.Definition as InternalDefinition).BuiltInParameter != BuiltInParameter.INVALID)
                            row.Add("BuiltInParameter");
                        else row.Add("FamilyParameter");

                        row.Add(par.CheckParameter(nestedFamDoc, paramAssDic));

                        table.Add(row);
                    }

                    var additionalNestedSPElements = new List<SharedParameterElement>();
                    using (var col = new FilteredElementCollector(nestedFamDoc).OfClass(typeof(SharedParameterElement)))
                    {
                        additionalNestedSPElements.AddRange(col.Where(x => !nestedFamGuidList.Any(z => z == (x as SharedParameterElement).GuidValue))
                            .Cast<SharedParameterElement>());
                    }

                    foreach (var para in additionalNestedSPElements)
                    {
                        var row = new List<string>();
                        row.Add(parentName + "/" + nestedFamDoc.Title);
                        row.Add(itemFam.FamilyCategory.Name);
                        row.Add(para.Name);
                        row.Add(para.GetDefinition().ParameterGroup.ToString());
                        row.Add(para.GuidValue.ToString());
                        row.Add(para.GetDefinition().ParameterType.ToString());
                        row.Add("SharedParameterElement");
                        row.Add("");

                        table.Add(row);
                    }
                    result.AddRange(table);

                    var nextNestedFamilies = new FilteredElementCollector(nestedFamDoc).OfClass(typeof(Family)).Where(x => (x as Family).IsEditable).Cast<Family>().ToList();

                    var nestedReport = ReportNestedFamilyParameters(nestedFamDoc, nextNestedFamilies, parentName + "/" + nestedFamDoc.Title, nestedParamAssDic);
                    if (nestedReport.Count != 0)
                    {
                        result.AddRange(nestedReport);
                    }
                }
                finally
                {
                    if (nestedFamDoc != null && nestedFamDoc.IsValidObject)
                    {
                        nestedFamDoc.Close(false);
                    }
                    else
                    {
                        failedFamilies += "nested_" + itemFam.Name + Environment.NewLine;
                    }
                }

            }

            return result;
        }
    }
}
