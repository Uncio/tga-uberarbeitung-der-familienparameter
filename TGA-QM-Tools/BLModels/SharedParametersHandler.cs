﻿using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TGA_QM_Tools.BLModels.SPF_logic;

namespace TGA_QM_Tools.BLModels
{
    public class SharedParametersHandler
    {
        private readonly Document document;
        private FamilyManager famManager;
        private ReadingSPF spfReader;

        public SharedParametersHandler(Document doc)
        {
            document = doc;
            famManager = doc.FamilyManager;
        }

        public FamilyParameter ReplaceWithSharedParameterSameType(FamilyParameter parameter, ExternalDefinition extDef)
        {
            try
            {
                var defToDelete = parameter.Definition as InternalDefinition;
                var tempParameter = famManager.ReplaceParameter(parameter, "tempParameter", parameter.Definition.ParameterGroup, parameter.IsInstance);
                if (defToDelete.IsValidObject)
                {
                    //todo or to delete shared parameter element?
                    document.Delete(defToDelete.Id);
                }
                var newParameter = famManager.ReplaceParameter(tempParameter, extDef, tempParameter.Definition.ParameterGroup, tempParameter.IsInstance);

                return newParameter;
            }
            catch
            {
                return null;
            }
        }

        public FamilyParameter ReplaceWithSharedParameterDifferentType(FamilyParameter parameter, ExternalDefinition extDef)
        {
            try
            {
                var defToDelete = parameter.Definition as InternalDefinition;
                var tempParameter = famManager.ReplaceParameter(parameter, "tempParameter", parameter.Definition.ParameterGroup, parameter.IsInstance);
                if (defToDelete.IsValidObject)
                {
                    document.Delete(defToDelete.Id);
                }
                var newParameter = ReplaceParametersDifTypes(tempParameter, extDef);

                return newParameter;
            }
            catch
            {
                return null;
            }
        }

        public FamilyParameter CreateFakeExistingParameter(SharedParameterElement sharedParameterElement)
        {
            if (spfReader == null)
            {
                spfReader = new ReadingSPF(document.Application);
                spfReader.CreateFakeSPF();
            }

            var extDef = spfReader.AddFakeExistingParameter(sharedParameterElement);        
            var famParameter = document.FamilyManager.AddParameter(extDef, sharedParameterElement.GetDefinition().ParameterGroup, true);

            return famParameter;
        }

        public FamilyParameter ReplaceSharedParameterWithDummyParameter(FamilyParameter parameter)
        {
            if (spfReader == null)
            {
                spfReader = new ReadingSPF(document.Application);
                spfReader.CreateFakeSPF();
            }

            var extDef = spfReader.AddFakeNewParameter(parameter);
            var oldDefinition = parameter.Definition;
            var tempParameter = document.FamilyManager.ReplaceParameter(parameter, "TEMP", parameter.Definition.ParameterGroup, parameter.IsInstance);
            if ((oldDefinition as InternalDefinition).IsValidObject)
                document.Delete((oldDefinition as InternalDefinition).Id);
            var newParameter = document.FamilyManager.ReplaceParameter(tempParameter, extDef, tempParameter.Definition.ParameterGroup, tempParameter.IsInstance);

            spfReader.RestoreOriginalSpf();

            return newParameter;
        }

        public FamilyParameter ReplaceWithFamilyParameterSameName(FamilyParameter parameter)
        {
            var name = parameter.Definition.Name;
            var definition = parameter.Definition;
            var newParameter = famManager.ReplaceParameter(parameter, "TempParameterName", parameter.Definition.ParameterGroup, parameter.IsInstance);
            famManager.RenameParameter(newParameter, name);
            if ((definition as InternalDefinition).IsValidObject)
            {
                document.Delete((definition as InternalDefinition).Id);
            }

            return newParameter;
        }

        private FamilyParameter ReplaceParametersDifTypes(FamilyParameter parameter, ExternalDefinition extDef)
        {
            var newParameter = famManager.AddParameter(extDef, parameter.Definition.ParameterGroup, parameter.IsInstance);
            // assigning formula
            if (!String.IsNullOrEmpty(parameter.Formula))
            {
                document.FamilyManager.SetFormula(newParameter, parameter.Formula);
            }
            if (famManager.IsParameterLocked(parameter))
                if (famManager.IsParameterLockable(newParameter))
                    famManager.SetParameterLocked(newParameter, true);
            // applying value and associate parameters to the new family parameter
            var famTypes = famManager.Types;
            foreach (FamilyType famType in famTypes)
            {
                famManager.CurrentType = famType;
                if (famType.HasValue(parameter) && newParameter.Formula == null)
                {
                    //todo to check how we take and apply vaklues
                    ParameterType parType = parameter.Definition.ParameterType;
                    if (parType == ParameterType.Text)
                    {
                        string defParValue = famManager.CurrentType.AsValueString(parameter);
                        if (defParValue != null) famManager.Set(newParameter, defParValue);
                    }
                    else if (parType == ParameterType.YesNo)
                    {
                        int defParValue = Int32.Parse(famManager.CurrentType.AsValueString(parameter));
                        famManager.Set(newParameter, defParValue);
                    }
                    else if (parType == ParameterType.Integer)
                    {
                        var defParValue = famManager.CurrentType.AsInteger(parameter);
                        if (newParameter.StorageType == StorageType.String)
                        {
                            famManager.Set(newParameter, defParValue.ToString());
                        }
                        else if (newParameter.StorageType == StorageType.Integer)
                            famManager.Set(newParameter, (int)defParValue);
                        else famManager.Set(newParameter, (double)defParValue / 304.8);
                    }
                    else
                    {
                        double defParValue = (double)famManager.CurrentType.AsDouble(parameter);
                        if (newParameter.StorageType == StorageType.String)
                        {
                            famManager.Set(newParameter, defParValue.ToString());
                        }
                        else famManager.Set(newParameter, defParValue);
                    }
                }
            }
            ParameterSet shPars1 = parameter.AssociatedParameters;
            var parametersToMove = new List<Parameter>();
            if (shPars1.Size != 0)
            {
                foreach (Parameter par in shPars1) parametersToMove.Add(par as Parameter);
            }
            foreach (Parameter par in parametersToMove)
            {
                famManager.AssociateElementParameterToFamilyParameter(par, newParameter);
            }
            var famParsCol = famManager.Parameters;
            //replacing changed parameters in formulas
            foreach (FamilyParameter item in famParsCol)
            {
                if (item.Formula != null && item.Formula.Contains(parameter.Definition.Name))
                {
                    var newFormula = item.Formula;
                    newFormula = newFormula.Replace(parameter.Definition.Name, newParameter.Definition.Name);
                    famManager.SetFormula(item, newFormula);
                }
            }

            //replacing changed parameters in dimension labels
            List<Element> dimsToChange = new FilteredElementCollector(document).OfClass(typeof(Dimension)).ToList();
            foreach (Element dim in dimsToChange)
            {
                FamilyParameter dimPar = null;
                try
                {
                    dimPar = (dim as Dimension).FamilyLabel;
                }
                catch { }
                if (dimPar != null && dimPar.Definition.Name == parameter.Definition.Name)
                {
                    (dim as Dimension).FamilyLabel = newParameter;
                }
            }
            return newParameter;
        }
    }
}
