﻿using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels.General
{
    class OpenDialogs
    {
        public static string[] GetFamiliesFromFolder()
        {
            //getting families
            var openDialog = new CommonOpenFileDialog();
            var initialPath = Properties.Settings.Default.FamilyFolder;
            openDialog.InitialDirectory = initialPath;
            openDialog.IsFolderPicker = true;
            openDialog.Title = "Familienordner auswählen...";
            if (openDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                Properties.Settings.Default.FamilyFolder = openDialog.FileName;
                Properties.Settings.Default.Save();
            }
            else
            {
                return null;
            }

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            return Directory.GetFiles(Properties.Settings.Default.FamilyFolder, "*.rfa", SearchOption.AllDirectories);
        }

        public static string GetFileDialog(string title, string initialPath, string filter)
        {
            var result = String.Empty;

            var openFileDialog = new OpenFileDialog();
            var initialFilePath = Properties.Settings.Default.CsvFilePath;
            openFileDialog.InitialDirectory = initialFilePath;
            openFileDialog.Title = title;
            openFileDialog.Filter = filter;
            if (openFileDialog.ShowDialog() == true)
            {
                result = openFileDialog.FileName;
            }
            else
            {
                return null;
            }

            return result;
        }
    }
}
