﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels.General
{
    public static class GeneralExtensions
    {
        public static void CheckExistingFamilies(this string path)
        {
            string folder = path;

            List<string> famPaths = Directory.GetFiles(folder, "*.rfa", SearchOption.AllDirectories).ToList();
            foreach (string file in famPaths)
            {
                string tempToCheck = file.Remove(file.IndexOf(".rfa"));
                tempToCheck = tempToCheck.Remove(0, tempToCheck.LastIndexOf('\\') + 1);
                if (tempToCheck.Contains('.')) File.Delete(file);
            }
        }

        public static void WriteCsvTo(this List<List<string>> table, string path, string fullPath = null)
        {
            if (table.Count > 1)
            {
                string dev = "\t";
                string[] csvLines = table.Select(x => string.Join(dev, x)).ToArray();

                if (!String.IsNullOrEmpty(fullPath))
                {
                    var t = Path.Combine(fullPath, path);
                    File.WriteAllLines(Path.Combine(fullPath, path), csvLines, Encoding.GetEncoding("iso-8859-1"));
                }
                else
                {
                    File.WriteAllLines(Path.Combine(Properties.Settings.Default.FamilyFolder, path), csvLines, Encoding.GetEncoding("iso-8859-1"));
                }
            }
        }

        public static void WriteToTxt(this string text, string path, string fullPath = null)
        {
            if (String.IsNullOrEmpty(fullPath))
            {
                fullPath = Path.Combine(Properties.Settings.Default.FamilyFolder, path);
            }
            else
            {
                fullPath = Path.Combine(fullPath, path);
            }

            if (File.Exists(path))
            {
                File.Delete(path);
            }
            if (!String.IsNullOrEmpty(text))
                File.WriteAllText(fullPath, text);
        }

        public static List<string> GetShortFileNames(this List<string> familyFiles) =>
            familyFiles.Select(x => Path.GetFileName(x)).Select(x => x.Remove(x.LastIndexOf(".rfa"))).ToList();

        public static List<string[]> ReadCsvFile(this string path)
        {
            var result = new List<string[]>();

            using (var sr = new StreamReader(Properties.Settings.Default.CsvFilePath, Encoding.GetEncoding("iso-8859-1")))
            {
                while (!sr.EndOfStream)
                {
                    var line = sr.ReadLine();
                    var values = line.Split(',');

                    result.Add(values);
                }
            }

            return result;
        }

        public static FamilyParameter ReplaceSharedParameterWithFake(this Document famDoc,
                                                                     SharedParameterElement spElement,
                                                                     List<FamilyParameter> famParameters,
                                                                     out string oldName,
                                                                     out bool toDelete)
        {
            FamilyParameter result = null;
            toDelete = false;

            var spHandler = new SharedParametersHandler(famDoc);

            using (var tr = new Transaction(famDoc, "fake parameter"))
            {
                tr.Start();

                var famParameter = famParameters.FirstOrDefault(x => x.IsShared && x.GUID == spElement.GuidValue);
                oldName = spElement.Name;

                if (famParameter == null)
                {
                    famParameter = spHandler.CreateFakeExistingParameter(spElement);
                    toDelete = true;
                }

                if (famParameter != null)
                {
                    try
                    {
                        result = spHandler.ReplaceSharedParameterWithDummyParameter(famParameter);
                    }
                    catch
                    {
                        tr.RollBack();
                    }
                }
                else
                {
                    tr.RollBack();
                }

                tr.Commit();
            }

            return result;
        }

        public static string CheckParameter(this FamilyParameter par, Document famDoc, Dictionary<string, List<string>> paramDic)
        {
            var result = "NotInUse";
            //parent assotiated parameters
            if (paramDic != null && paramDic.Values.ToList().Where(x => x.Contains(par.Definition.Name)).Count() != 0)
                return String.Empty;

            //assotiated params
            if (par.AssociatedParameters.Size != 0)
                return String.Empty;

            //inside formulas
            var parameters = famDoc.FamilyManager.Parameters;
            foreach (FamilyParameter para in parameters)
            {
                if (!String.IsNullOrEmpty(para.Formula) && para.Formula.Contains(par.Definition.Name))
                    return String.Empty;
            }

            //in dimentions
            List<Element> dimsToChange = new FilteredElementCollector(famDoc).OfClass(typeof(Dimension)).ToList();
            foreach (Element dim in dimsToChange)
            {
                FamilyParameter dimPar = null;
                try
                {
                    dimPar = (dim as Dimension).FamilyLabel;
                }
                catch { }
                if (dimPar != null && dimPar.Definition.Name == par.Definition.Name)
                {
                    return String.Empty;
                }
            }
            return result;
        }
    }
}
