﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels
{
    public class ParameterAssociation
    {
        public string ParameterName { get; set; }
        public bool IsShared { get; set; }
        public ElementId ParameterId { get; set; }
        public Guid ParameterGuid { get; set; }
        public ElementId AssociatedElementId { get; set; }
        public List<ParameterAssociation> AssociatedParameters { get; set; }

        public ParameterAssociation(FamilyParameter parameter)
        {
            ParameterName = parameter.Definition.Name;
            IsShared = parameter.IsShared;
            ParameterId = parameter.Id;

            if (IsShared)
                ParameterGuid = parameter.GUID;

            AssociatedParameters = new List<ParameterAssociation>();
            foreach (Parameter assPar in parameter.AssociatedParameters)
            {
                AssociatedParameters.Add(new ParameterAssociation(assPar));
            }
        }

        public ParameterAssociation(FamilyParameter parameter, List<Parameter> associatedParameters)
        {
            ParameterName = parameter.Definition.Name;
            IsShared = parameter.IsShared;
            ParameterId = parameter.Id;

            if (IsShared)
                ParameterGuid = parameter.GUID;

            AssociatedParameters = new List<ParameterAssociation>();
            associatedParameters.ForEach(x => AssociatedParameters.Add(new ParameterAssociation(x)));
        }

        public ParameterAssociation(Parameter parameter)
        {
            ParameterName = parameter.Definition.Name;
            IsShared = parameter.IsShared;
            if (IsShared)
                ParameterGuid = parameter.GUID;
            AssociatedElementId = parameter.Element.Id;
        }
    }
}
