﻿using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels.FailureHandlers
{
    public class FailureHandling : IFailuresPreprocessor
    {
        public static bool Success { get; set; } = true;
        public FailureProcessingResult PreprocessFailures(FailuresAccessor failuresAccessor)
        {
            var failList = failuresAccessor.GetFailureMessages();
            foreach (FailureMessageAccessor failure in failList)
            {
                var temp = failure.GetSeverity();
                if (temp == FailureSeverity.Error)
                {
                    Success = false;
                }
                failuresAccessor.DeleteWarning(failure);
                return FailureProcessingResult.ProceedWithRollBack;
            }

            return FailureProcessingResult.Continue;
        }
    }
}
