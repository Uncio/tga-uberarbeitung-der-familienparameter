﻿using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TGA_QM_Tools.BLModels.SPF_logic
{
    public class ReadingSPF
    {
        private readonly Application application;
        private string originalSpf;
        private string fakeSPF;
        private DefinitionFile spf;
        private bool spfChanged = false;

        private List<ExternalDefinition> definitions;
        public List<ExternalDefinition> Definitions 
        { 
            get
            {
                if (definitions == null)
                {
                    definitions = new List<ExternalDefinition>();
                }
                if (definitions.Any() && !spfChanged)
                {
                    return definitions;
                }
                else if (!String.IsNullOrEmpty(application.SharedParametersFilename))
                {
                    definitions = ReadAllDefinitions();
                    spfChanged = false;
                    return definitions;
                }
                else
                {
                    return new List<ExternalDefinition>();
                }
            }
        }
        public ReadingSPF(Application app)
        {
            application = app;
            originalSpf = app.SharedParametersFilename;
            if (!String.IsNullOrEmpty(originalSpf))
            {
                try
                {
                    spf = app.OpenSharedParameterFile();
                    spfChanged = true;
                }
                catch { }
            }
        }

        public bool ChangeSPF(string path)
        {
            application.SharedParametersFilename = path;
            try
            {
                spf = application.OpenSharedParameterFile();
                spfChanged = true;
            }
            catch
            {
                return false;
            }
            return true;
        }

        private List<ExternalDefinition> ReadAllDefinitions()
        {
            var result = new List<ExternalDefinition>();

            var groups = spf.Groups;
            foreach(var group in groups)
            {
                foreach(var def in group.Definitions)
                {
                    result.Add(def as ExternalDefinition);
                }
            }

            return result;
        }

        public void CreateFakeSPF()
        {
            var tempFolder = Path.GetTempPath();
            var file = Properties.Resources.SPF_helper;            

            var filePath = Path.Combine(tempFolder, "SPF_helper.txt");
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
            File.WriteAllText(filePath, file);

            application.SharedParametersFilename = filePath;
            spf = application.OpenSharedParameterFile();
            spfChanged = true;
        }

        public ExternalDefinition AddFakeExistingParameter(SharedParameterElement element)
        {
            var definitions = spf.Groups.FirstOrDefault().Definitions;
            var options = new ExternalDefinitionCreationOptions(element.Name, element.GetDefinition().ParameterType)
            {
                GUID = element.GuidValue
            };
            return definitions.Create(options) as ExternalDefinition;
        }

        public ExternalDefinition AddFakeNewParameter(FamilyParameter parameter)
        {
            var definitions = spf.Groups.FirstOrDefault().Definitions;
            var options = new ExternalDefinitionCreationOptions(parameter.Definition.Name + "_dummy", parameter.Definition.ParameterType)
            {
                GUID = Guid.NewGuid()
            };
            return definitions.Create(options) as ExternalDefinition;
        }

        public void RestoreOriginalSpf()
        {
            if (!String.IsNullOrEmpty(originalSpf))
            {
                application.SharedParametersFilename = originalSpf;
            }
        }
    }
}
