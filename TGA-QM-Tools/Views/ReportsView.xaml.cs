﻿using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TGA_QM_Tools.BLModels.General;
using TGA_QM_Tools.BLModels.Reports;

namespace TGA_QM_Tools.Views
{
    /// <summary>
    /// Interaction logic for ReportsView.xaml
    /// </summary>
    public partial class ReportsView : Window
    {
        private readonly UIApplication application;
        public ReportsView(UIApplication uIApplication)
        {
            application = uIApplication;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var ui = new CustomUIVM("Familienbericht (mit Parametern)", new List<FileFolderModel>(), Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                Close();
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;

            if (familyFiles == null)
            {
                Close();
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            new FamiliesParametersReport().Report(application.Application, familyFiles, prefix);

            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var ui = new CustomUIVM("Family Shared Parameters Report", new List<FileFolderModel>(), Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                Close();
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;

            if (familyFiles == null)
            {
                Close();
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            new FamiliesParametersReport2().Report(application.Application, familyFiles, prefix);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var ui = new CustomUIVM("Sichtbarkeitsbericht", new List<FileFolderModel>(), Properties.Settings.Default.FamilyFolder);
            if (ui.IsCancelled)
            {
                Close();
            }

            Properties.Settings.Default.FamilyFolder = ui.DialogSettings[0].Path;
            Properties.Settings.Default.Save();

            Properties.Settings.Default.FamilyFolder.CheckExistingFamilies();

            var familyFiles = ui.FamilyFiles;

            if (familyFiles == null)
            {
                Close();
            }

            var selectedParts = ui.Part1.Where(x => x.IsSelected).Select(x => x.Name).ToList();
            selectedParts.AddRange(ui.Part2.Where(x => x.IsSelected).Select(x => x.Name));
            var prefix = String.Empty;
            selectedParts.ForEach(x => prefix += x + "_");

            new VisibilityReport().Report(application.Application, familyFiles, prefix);

            Close();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            var additionalDialogs = new List<FileFolderModel>
            {
                new FileFolderModel {
                    Path = Properties.Settings.Default.TgaSpfFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "txt files(*.txt) | *.txt",
                    Label = "TGA SPF-Datei auswählen..."},
                new FileFolderModel {
                    Path = Properties.Settings.Default.BauSpfFilePath,
                    DialogType = DialogTypeEnum.File,
                    Filter = "txt files(*.txt) | *.txt",
                    Label = "BAU SPF-Datei auswählen..."},
                new FileFolderModel {
                    Path = Properties.Settings.Default.ReportFolder,
                    DialogType = DialogTypeEnum.Folder,
                    Label = "Ordner zum Speichern des Berichts ..."},
            };

            var ui = new CustomUIVM2("TemplateParametersReport", additionalDialogs);
            if (ui.IsCancelled)
            {
                Close();
            }

            Properties.Settings.Default.TgaSpfFilePath = ui.DialogSettings[0].Path;
            Properties.Settings.Default.BauSpfFilePath = ui.DialogSettings[1].Path;
            Properties.Settings.Default.ReportFolder = ui.DialogSettings[2].Path;
            Properties.Settings.Default.Save();

            var templateParametersUtil = new TemplateParametersReport(application);
            templateParametersUtil.CollectData(Properties.Settings.Default.TgaSpfFilePath, Properties.Settings.Default.BauSpfFilePath);
            templateParametersUtil.Report(Properties.Settings.Default.ReportFolder);

            Close();
        }
    }
}
