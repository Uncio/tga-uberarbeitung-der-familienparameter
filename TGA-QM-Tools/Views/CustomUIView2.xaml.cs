﻿using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace TGA_QM_Tools.Views
{
    /// <summary>
    /// Interaction logic for CustomUIView2.xaml
    /// </summary>
    public partial class CustomUIView2 : Window
    {
        public CustomUIView2(string title)
        {
            InitializeComponent();
            Title = title;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var item = ((sender as Button).TemplatedParent as ContentPresenter).Content as FileFolderModel;

            switch (item.DialogType)
            {
                case DialogTypeEnum.File:
                    {
                        var openFileDialog = new OpenFileDialog();
                        openFileDialog.InitialDirectory = item.Path;
                        openFileDialog.Title = item.Label;
                        openFileDialog.Filter = item.Filter;
                        if (openFileDialog.ShowDialog() == true)
                        {
                            item.Path = openFileDialog.FileName;
                        }
                    }
                    break;
                case DialogTypeEnum.Folder:
                    {
                        var openDialog = new CommonOpenFileDialog();
                        openDialog.InitialDirectory = item.Path;
                        openDialog.IsFolderPicker = true;
                        openDialog.Title = item.Label;
                        if (openDialog.ShowDialog() == CommonFileDialogResult.Ok)
                        {
                            item.Path = openDialog.FileName;
                        }
                    }
                    break;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }

    public class CustomUIVM2 : INotifyPropertyChanged
    {
        private CustomUIView2 view;
        public ObservableCollection<FileFolderModel> DialogSettings { get; set; }

        public bool IsCancelled { get; set; } = false;

        public CustomUIVM2(string title, List<FileFolderModel> dialogs)
        {
            InitializeData(dialogs);
            InitializeUI(title);
        }

        private void InitializeData(List<FileFolderModel> additionalDialogs)
        {
            DialogSettings = new ObservableCollection<FileFolderModel>();

            foreach (var dialog in additionalDialogs)
            {
                DialogSettings.Add(dialog);
            }
        }

        private void InitializeUI(string title)
        {
            view = new CustomUIView2(title);
            view.DataContext = this;
            view.ShowDialog();

            if (view.DialogResult == true)
            {

            }
            else
            {
                IsCancelled = true;
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

