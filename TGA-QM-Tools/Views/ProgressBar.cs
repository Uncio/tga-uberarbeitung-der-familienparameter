﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TGA_QM_Tools.Views
{
    public partial class ProgressBar : Form
    {
        /// <summary>
        /// Example:
        /// title = "Process files";
        /// var maxValue = files.Count;
        /// string s = "{0} of " + maxValue.ToString() + " files processed...";
        /// using(ProgressBar pf = new ProgressBar(title, s, maxValue))
        /// {
        ///     foreach (var file in files)
        ///     {
        ///         var = DoStuff();
        ///         pf.Increment();
        ///     }
        /// }
        /// </summary>
        string _format;
        public ProgressBar(string caption, string format, int max)
        {
            _format = format;
            InitializeComponent();
            Text = caption;
            label1.Text = (null == format) ? caption : string.Format(format, 0);
            progressBar1.Minimum = 0;
            progressBar1.Maximum = max;
            progressBar1.Value = 0;
            Show();
            Application.DoEvents();
        }
        public void Increment()
        {
            ++progressBar1.Value;
            if (null != _format)
            {
                label1.Text = string.Format(_format, progressBar1.Value);
            }
            Application.DoEvents();
        }
#if USE_MARTINS_PROGRESS_FORM
    public void SetText(string text)
    {
      label1.Text = text;
      System.Windows.Forms.Application.DoEvents();
    }

    public void SetProgressBarMinMax(int min, int max)
    {
      progressBar1.Minimum = min;
      progressBar1.Maximum = max;
      progressBar1.Value = 0;
    }

    public void IncrementProgressBar()
    {
      progressBar1.Value++;
      System.Windows.Forms.Application.DoEvents();
    }

    public void HideProgressBar()
    {
      progressBar1.Visible = false;
      System.Windows.Forms.Application.DoEvents();
    }

    public void ShowProgressBar()
    {
      progressBar1.Visible = true;
      System.Windows.Forms.Application.DoEvents();
    }
#endif // USE_MARTINS_PROGRESS_FORM
    }
}
