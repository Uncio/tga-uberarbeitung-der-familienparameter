﻿using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TGA_QM_Tools.Views
{
    /// <summary>
    /// Interaction logic for CustomUIView.xaml
    /// </summary>
    public partial class CustomUIView : Window
    {
        public CustomUIView(string title)
        {
            InitializeComponent();
            Title = title;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var item = ((sender as Button).TemplatedParent as ContentPresenter).Content as FileFolderModel;

            switch (item.DialogType)
            {
                case DialogTypeEnum.File:
                    {
                        var openFileDialog = new OpenFileDialog();
                        openFileDialog.InitialDirectory = item.Path;
                        openFileDialog.Title = item.Label;
                        openFileDialog.Filter = item.Filter;
                        if (openFileDialog.ShowDialog() == true)
                        {
                            item.Path = openFileDialog.FileName;
                        }
                    }
                    break;
                case DialogTypeEnum.Folder:
                    {
                        var openDialog = new CommonOpenFileDialog();
                        openDialog.InitialDirectory = item.Path;
                        openDialog.IsFolderPicker = true;
                        openDialog.Title = item.Label;
                        if (openDialog.ShowDialog() == CommonFileDialogResult.Ok)
                        {
                            (DataContext as CustomUIVM).FamilyFiles = Directory.GetFiles(openDialog.FileName, item.Filter, SearchOption.AllDirectories).ToList();
                            item.Path = openDialog.FileName;
                        }
                    }
                    break;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void ListBox_Loaded(object sender, RoutedEventArgs e)
        {
            var temp = (sender as ListBox).Items;
        }
    }

    public class CustomUIVM : INotifyPropertyChanged
    {
        private CustomUIView view;
        public ObservableCollection<FileFolderModel> DialogSettings { get; set; }
        public ObservableCollection<PartSettingVM> Part1 { get; set; }
        public ObservableCollection<PartSettingVM> Part2 { get; set; }

        public bool IsCancelled { get; set; } = false;
        public List<string> FamilyFiles { get; set; }
        public Dictionary<string, List<string>> FamilyFilesDic { get; set; }

        public CustomUIVM(string title, List<FileFolderModel> dialogs, string initialFamiliesFolder)
        {
            InitializeData(dialogs, initialFamiliesFolder);
            InitializeUI(title);
        }

        private void InitializeData(List<FileFolderModel> additionalDialogs, string initialFamiliesFolder)
        {
            DialogSettings = new ObservableCollection<FileFolderModel>();
            DialogSettings.Add(new FileFolderModel { Filter = "*.rfa", Label = "Select families folder...", Path = initialFamiliesFolder, DialogType = DialogTypeEnum.Folder });
            foreach (var dialog in additionalDialogs)
            {
                DialogSettings.Add(dialog);
            }

            Part1 = new ObservableCollection<PartSettingVM>();
            Part2 = new ObservableCollection<PartSettingVM>();

            for (int i = 1; i < 5; i++)
            {
                Part1.Add(new PartSettingVM("Part " + i.ToString()));
                Part2.Add(new PartSettingVM("Part " + (i + 4).ToString()));
            }

            FamilyFiles = new List<string>();
        }

        private void InitializeUI(string title)
        {
            view = new CustomUIView(title);
            view.DataContext = this;
            view.ShowDialog();

            if (view.DialogResult == true)
            {
                RecollectFilesInParts();
            }
            else
            {
                IsCancelled = true;
            }
        }

        private void RecollectFilesInParts()
        {
            List<string> queue = new List<string>();

            if (FamilyFiles.Count == 0)
            {
                FamilyFiles = Directory.GetFiles(DialogSettings.FirstOrDefault().Path, DialogSettings.FirstOrDefault().Filter, SearchOption.AllDirectories).ToList();
            }

            FamilyFiles.Sort((x, y) => { return x.CompareTo(y); });

            List<string> tempList = new List<string>();
            int step = FamilyFiles.Count / 8;

            FamilyFilesDic = new Dictionary<string, List<string>>();
            FamilyFilesDic.Add("Part 1", new List<string>());
            FamilyFilesDic.Add("Part 2", new List<string>());
            FamilyFilesDic.Add("Part 3", new List<string>());
            FamilyFilesDic.Add("Part 4", new List<string>());
            FamilyFilesDic.Add("Part 5", new List<string>());
            FamilyFilesDic.Add("Part 6", new List<string>());
            FamilyFilesDic.Add("Part 7", new List<string>());
            FamilyFilesDic.Add("Part 8", new List<string>());

            if (FamilyFiles.Count() <= 8)
            {
                FamilyFilesDic["Part 1"].AddRange(FamilyFiles);
            }
            else
            {
                FamilyFilesDic["Part 1"].AddRange(FamilyFiles.GetRange(0, step));
                FamilyFilesDic["Part 2"].AddRange(FamilyFiles.GetRange(step, step));
                FamilyFilesDic["Part 3"].AddRange(FamilyFiles.GetRange(2 * step, step));
                FamilyFilesDic["Part 4"].AddRange(FamilyFiles.GetRange(3 * step, step));
                FamilyFilesDic["Part 5"].AddRange(FamilyFiles.GetRange(4 * step, step));
                FamilyFilesDic["Part 6"].AddRange(FamilyFiles.GetRange(5 * step, step));
                FamilyFilesDic["Part 7"].AddRange(FamilyFiles.GetRange(6 * step, step));
                FamilyFilesDic["Part 8"].AddRange(FamilyFiles.GetRange(7 * step, FamilyFiles.Count - 7 * step));
            }


            foreach (var part in Part1.Where(x => x.IsSelected))
            {
                queue.AddRange(FamilyFilesDic[part.Name]);
            }
            foreach (var part in Part2.Where(x => x.IsSelected))
            {
                queue.AddRange(FamilyFilesDic[part.Name]);
            }

            queue.Sort((x, y) => { return x.CompareTo(y); });
            FamilyFiles.Clear();
            FamilyFiles = queue;
        }



        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum DialogTypeEnum
    {
        Folder,
        File
    }
    public class FileFolderModel : INotifyPropertyChanged
    {
        public string Label { get; set; }
        public DialogTypeEnum DialogType { get; set; }
        public string Filter { get; set; }

        private string path;
        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                path = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class PartSettingVM : INotifyPropertyChanged
    {
        public string Name { get; set; }
        public bool IsSelected { get; set; } = true;

        public PartSettingVM(string name)
        {
            Name = name;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
