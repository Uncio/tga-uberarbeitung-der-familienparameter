﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace X_rayvit
{
    class QueueCreator 
    {
        
        public List<string> CreateQueue(string folder_path, List<string> parts)
        {
            List<string> queue = new List<string>();
            List<string> jsonFiles = new List<string>();
            jsonFiles.AddRange(Directory.GetFiles(folder_path, "*.rfa", SearchOption.AllDirectories));
            jsonFiles.Sort(CompareByName);
            // sorting files for lists of one generic model files
            List<string> tempList = new List<string>();
            int step = jsonFiles.Count / 8;
            
            Dictionary<string, List<string>> tempJsonFiles = new Dictionary<string, List<string>>();

            tempJsonFiles.Add("Part 1", new List<string>());
            tempJsonFiles.Add("Part 2", new List<string>());
            tempJsonFiles.Add("Part 3", new List<string>());
            tempJsonFiles.Add("Part 4", new List<string>());
            tempJsonFiles.Add("Part 5", new List<string>());
            tempJsonFiles.Add("Part 6", new List<string>());
            tempJsonFiles.Add("Part 7", new List<string>());
            tempJsonFiles.Add("Part 8", new List<string>());
            
            if (jsonFiles.Count() <= 8)
            {
                tempJsonFiles["Part 1"].AddRange(jsonFiles);
            }
            else
            {
                tempJsonFiles["Part 1"].AddRange(jsonFiles.GetRange(0, step));
                tempJsonFiles["Part 2"].AddRange(jsonFiles.GetRange(step, step));
                tempJsonFiles["Part 3"].AddRange(jsonFiles.GetRange(2*step, step));
                tempJsonFiles["Part 4"].AddRange(jsonFiles.GetRange(3*step, step));
                tempJsonFiles["Part 5"].AddRange(jsonFiles.GetRange(4*step, step));
                tempJsonFiles["Part 6"].AddRange(jsonFiles.GetRange(5*step, step));
                tempJsonFiles["Part 7"].AddRange(jsonFiles.GetRange(6*step, step));
                tempJsonFiles["Part 8"].AddRange(jsonFiles.GetRange(7*step, jsonFiles.Count - 7 * step));
            }

            
            if (parts.Count() != 0)
            {
                foreach (var item in parts)
                    queue.AddRange(tempJsonFiles[item]);
                queue.Sort(CompareByName);
            }

            return queue;
        }
        private static int CompareByName(string x, string y)
        {
            int retval = x.CompareTo(y);
            return retval;
        }
    }
}

